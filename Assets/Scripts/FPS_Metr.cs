using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class FPS_Metr : MonoBehaviour
{

    private float _time;
    private int _fps;
    private Text _text;

    private void Start()
    {
        _time = Time.time;
        _text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        _fps++;
        if (Time.time - _time >= 1f)
        {
            _text.text = _fps.ToString();
            _fps = 0;
            _time = Time.time;
        }
    }
}
