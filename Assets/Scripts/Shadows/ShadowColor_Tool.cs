using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowColor_Tool : Tool, IDrawTool
{
    public override void Start()
    {
        base.Start();
        DeltaLeftPosition = 0.09f;
        DrawTool = this;
        IsEraser = true;
        foreach (var shadow in Data.ShadowsData.ShadowStandards)
        {
            GameObject obj = Instantiate(Data.ShadowsData.Prefab, DownMenu.ParentCuvet);
            obj.transform.localPosition=Vector3.zero;
            Item item = obj.GetComponent<Item>();
            Items.Add(item);
            item.Init(this, shadow.Icon);
            item.SetSprite(shadow.Sprite);
            item.SetLockNumber(shadow.LockNumber);
            item.SetType(ManagersEnum.ShadowsStandart);
        }
    }

    public override void Activate()
    {
        base.Activate();
        DownMenu.UpCuvettePanel(this);
        Manager.ActivateTutor();
    }
    
    public void Draw()
    {
        if (SelectedItem)
        {
            foreach (var targetImage in TargetImages)
            {
                Paint.DrawImage(targetImage, SelectedItem.Sprite);
                GameManager.Instance.Winking.SwitchWink(true);
            }
        }
    }
}
