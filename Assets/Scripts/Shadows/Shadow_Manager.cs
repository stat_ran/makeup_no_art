﻿
public class Shadow_Manager : Manager
{
    public override void Start()
    {
        base.Start();
        foreach (var tool in Tools)
        {
            tool.Init(this);
        }

        Clear();
    }

    // private void Update()
    // {
    //     if (!Input.GetMouseButton(0))
    //     {
    //         GM.Winking.IsWinkAllowed = true;
    //         GM.Winking.SwitchWink(false);
    //         return;
    //     }
    //     if(Input.mousePosition.x>1000000) return;
    //     RaycastHit2D hit2D=Physics2D.Raycast(MyCamera.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, Single.MinValue, 1<<9);
    //
    //     if (hit2D.collider!=null && hit2D.collider.name=="Shadows")
    //     {
    //         GM.Winking.IsWinkAllowed = false;
    //         GM.Winking.SwitchWink(true);
    //     }
    //     else
    //     {
    //         GM.Winking.IsWinkAllowed = true;
    //         GM.Winking.SwitchWink(false);
    //     }
    // }
    
}
