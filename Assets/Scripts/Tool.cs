﻿using System;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Tool : BaseBahavior
{

    public Sprite CursorTool_Sprite;
    
    [HideInInspector] public Item SelectedItem;
    [HideInInspector] public Sprite SelectedImage;
    [HideInInspector] public Color SelectedColor;
    [HideInInspector] public List<SpriteRenderer> TargetImages=new List<SpriteRenderer>();
    [HideInInspector] public Vector2 Offset;
    [HideInInspector] public Image Outline;
    [HideInInspector] public Image IconTool;
    [HideInInspector] public Vector2 PrevPos;
    [HideInInspector] public bool IsMovementAllowed;
    [HideInInspector] public bool IsActive;
    [HideInInspector] public bool IsEraser;
    [HideInInspector] public IDrawTool DrawTool;
    [HideInInspector] public List<Item> Items = new List<Item>();
    [HideInInspector] public Manager Manager;
    [HideInInspector] public Image LockImage;
    [HideInInspector] public bool IsLock;
    [HideInInspector] public int LockNumber;
    [HideInInspector] public ManagersEnum ManagersEnum;
    [HideInInspector] public float DeltaLeftPosition;
    [HideInInspector] public RectTransform RectTransform;

    private Vector3 _startPos;
    private float _moveVelocity = 40f;
    private int _countClearPoint;
    private Transform _transform;
    private Vector2 _vector2=Vector2.one;
    private float _speed = 30f;

    private float _time;

    public override void Awake()
    {
        base.Awake();
        GetComponent<Button>()?.onClick.AddListener(Activate);
        RectTransform = GetComponent<RectTransform>();
        MyAwake();
    }

    public void MyAwake()
    {
        Outline = GetComponent<Image>();
        if(Outline) Outline.color = GM.ColorOutlineTools;
        if(transform.childCount>0) IconTool = transform.GetChild(0).GetComponent<Image>();
        if (transform.childCount > 1) LockImage = transform.GetChild(1).GetComponent<Image>();
        transform.localPosition=Vector3.zero;
    }

    public override void Start()
    {
        base.Start();
        
    }

    private void Update()
    {
        if(!DownMenu.Manager || DownMenu.Manager.Tool != this) return;
        if (Outline && Outline.color==Color.white) // && DownMenu.Manager && DownMenu.Manager.Tool == this
        {
            _vector2.x = transform.position.x;
            _vector2.y = DownMenu.ArrowDown.position.y;
            DownMenu.ArrowDown.position=_vector2;
        }

        if(DownMenu.IsClear) return;
        
        if (Input.GetMouseButtonDown(0)) //DownMenu.Manager && DownMenu.Manager.Tool == this && 
        {
            if (PointerRaycast(MousePosition))
            {
                if (CursorTool_Sprite && !DownMenu.CursorTool.activeSelf)
                {
                    //Активируем префаб для курсора и ставим в начальную точку
                    DownMenu.CursorTool.SetActive(true);
                    DownMenu.CursorTool.transform.position = transform.position;
                    DownMenu.CursorTool.GetComponentInChildren<SpriteRenderer>().sprite = CursorTool_Sprite;
                }
            }
        }

        if ( Input.GetMouseButton(0)) //DownMenu.Manager && DownMenu.Manager.Tool == this &&
        {
            if (PointerRaycast(MousePosition))
            {
                if (CursorTool_Sprite && DownMenu.CursorTool.activeSelf)
                {
                    //Если префаб активирован то начинаем движение
                    DownMenu.CursorTool.transform.DOKill();
                    Vector2 direct = MousePosition - (Vector2)DownMenu.CursorTool.transform.position;
                    DownMenu.CursorTool.transform.position = Vector2.Lerp(DownMenu.CursorTool.transform.position,
                        MousePosition, Time.deltaTime * _speed);
                }
            }
            else
            {
                if (DownMenu.CursorTool.activeSelf && !DOTween.IsTweening(DownMenu.CursorTool.transform))
                {
                    DownMenu.CursorTool.transform.DOMove(transform.position, 0.2f).SetEase(Ease.Linear).onComplete +=
                        HideCursorTool;
                }
            }
        }

        if (Input.GetMouseButtonUp(0)) //DownMenu.Manager && DownMenu.Manager.Tool == this && 
        {
            if (PointerRaycast(MousePosition))
            {
                if (CursorTool_Sprite && DownMenu.CursorTool.activeSelf)
                {
                    //Если префаб был активирован то двигаемся назад к инструменту
                    DownMenu.CursorTool.transform.DOMove(transform.position, 0.2f).SetEase(Ease.Linear).onComplete +=
                        HideCursorTool;
                }
            }
            
        }
    }
    
    bool PointerRaycast(Vector2 position)
    {
        PointerEventData pointerData = new PointerEventData(EventSystem.current);
        pointerData.position = position;
       
#if !UNITY_EDITOR
		if(EventSystem.current.IsPointerOverGameObject(0)) return false;
		else return true;
#endif
        
#if UNITY_EDITOR
        if (EventSystem.current.IsPointerOverGameObject()) return false;
        else return true;
#endif
    }

    private void HideCursorTool()
    {
        DownMenu.CursorTool.SetActive(false);
    }

    public virtual void Init(Manager manager)
    {
        Manager = manager;
        for (int i = 0; i < Manager.TargetSpriteRenderers.Length; i++)
        {
            TargetImages.Add(Manager.TargetSpriteRenderers[i]);
        }
        gameObject.SetActive(false);
    }

    public virtual void MoveComplete()
    {
        IsMovementAllowed = true;
    }

    #region XXX______XXX______XXX

    public virtual void Activate()
    {
        DownMenu.StopAnimArrow();
        if (DownMenu.Manager.Tool is { } && DownMenu.Manager.Tool == this)
        {
            DownMenu.Eraser.Back();
            return;
        }
        if(DownMenu.Manager.Tool) DownMenu.Manager.Tool.Deactivate();
        DownMenu.Manager.Tool = this;
        ActiveOutline();
        DownMenu.Eraser.Back();
        transform.localScale=Vector3.one*1.2f;
        if (Items.Count > 0 && !SelectedItem) Items[0].Activate();
    }

    public virtual void Deactivate()
    {
        DeactivateOutline();
        transform.localScale=Vector3.one;
        if(Manager) Manager.ClearTutor();
    }

    private void ActiveOutline()
    {
        if (Outline) Outline.color = Color.white;
    }

    private void DeactivateOutline()
    {
        if (Outline) Outline.color = GM.ColorOutlineTools;
    }

    #endregion
    

    void MoveEnd()
    {
        IsMovementAllowed = true;
    }

    public void PlayEffect()
    {
        if(GameManager.Instance.Particle.isEmitting) return;
        GameManager.Instance.Particle.Play(true);
    }

    public void StopEffect()
    {
        if(!GameManager.Instance.Particle.isEmitting) return;
        GameManager.Instance.Particle.Stop(true, ParticleSystemStopBehavior.StopEmitting);
    }

    public virtual void SetItem(Item item)
    {
        SelectedItem = item;
    }

    public void MoveToolToCursor()
    {
        
    }

    public void BackToolCursorToTool()
    {
        
    }
    
    public void CheckBuy(object obj)
    {
        if (PlayerPrefs.HasKey(GetName()) || MySDK.Instance.NoAds || LockNumber==0)
        {
            LockImage.enabled = false;
            IsLock = false;
        }
        else
        {
            LockImage.enabled = true;
            IsLock = true;
        }

        
        if (obj is Tool tool && tool == this)
        {
            Activate();
        }
    }
    
    public string GetName()
    {
        return "Lock" + ManagersEnum + LockNumber;
    }

}
