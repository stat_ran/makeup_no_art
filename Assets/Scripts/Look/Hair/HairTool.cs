﻿using System.CodeDom;
using System.Runtime.CompilerServices;
using AceCream.Scripts.Notification;
using UnityEngine;
using UnityEngine.UI;

public class HairTool : Tool
{
    [HideInInspector] public SpriteRenderer HairBack;  //Ссылка на Волосы перса
    [HideInInspector] public SpriteRenderer HairFront; //Ссылка на Волосы перса

    private Sprite _front;
    private Sprite _back;

    public override void Start()
    {
        base.Start();

        IsMovementAllowed = false;
        NotificationCenter.addCallback(Notifications.CheckBuy, CheckBuy);
    }
    
    private void OnDestroy()
    {
        NotificationCenter.removeCallbeck(Notifications.CheckBuy, CheckBuy);
    }

    public void Init(SpriteRenderer frontRenderer, SpriteRenderer backRenderer, Sprite front, Sprite back, Sprite icon, int lockNumber)
    {
        MyAwake();
        _front = front;
        _back = back;
        HairBack = backRenderer;
        HairFront = frontRenderer;
        IconTool.sprite = icon;
        LockNumber = lockNumber;
        ManagersEnum = ManagersEnum.Hair;
        CheckBuy(null);
        gameObject.SetActive(false);
    }

    public override void Activate()
    {
        if (IsLock)
        {
            MyShowRewardAds.Instance.Activate(GetName(), this);
            return;
        }
        base.Activate();
        HairBack.sprite = _back;
        HairFront.sprite = _front;
        DownMenu.StartAnimArrow();
    }
    
}
