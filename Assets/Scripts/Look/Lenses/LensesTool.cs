﻿using System;
using AceCream.Scripts.Notification;
using UnityEngine;
using UnityEngine.UI;

public class LensesTool : Tool
{
    [HideInInspector] public SpriteRenderer[] Lenses;

    private Sprite _lens;

    public override void Start()
    {
        base.Start();
        IsMovementAllowed = false;
        NotificationCenter.addCallback(Notifications.CheckBuy, CheckBuy);
    }

    private void OnDestroy()
    {
        NotificationCenter.removeCallbeck(Notifications.CheckBuy, CheckBuy);
    }

    public void Init(SpriteRenderer[] lensRenderer, Sprite lens, int lockNumber)
    {
        MyAwake();
        ManagersEnum = ManagersEnum.Lens;
        _lens = lens;
        Lenses = lensRenderer;
        LockNumber = lockNumber;
        IconTool.sprite = lens;
        CheckBuy(null);
        gameObject.SetActive(false);
    }

    public override void Activate()
    {
        
        if (IsLock)
        {
            MyShowRewardAds.Instance.Activate(GetName(), this);
            return;
        }
        base.Activate();
        foreach (var lense in Lenses)
        {
            lense.sprite = _lens;
        }
        DownMenu.StartAnimArrow();
    }
    
}
