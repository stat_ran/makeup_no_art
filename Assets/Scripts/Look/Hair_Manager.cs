﻿using UnityEngine;

public class Hair_Manager : Manager
{
    [Header("Hairs Settings")]
    public SpriteRenderer HairBack;
    public SpriteRenderer HairFront;
    
    public override void Start()
    {
        base.Start();
        DeltaLeftPosition = 0.045f;
        IsSpawnTools = true;
        foreach (var hair in Data.HairData.Hairs)
        {
            GameObject obj = Instantiate(Data.HairData.Prefab, DownMenu.ParentTools);
            obj.transform.localPosition=Vector3.zero;
            obj.GetComponent<HairTool>().Init(HairFront, HairBack, hair.Front, hair.Back, hair.Icon, hair.LockNumber);
            Tools.Add(obj.GetComponent<Tool>());
        }
    }

    public override void Randomizer()
    {
        base.Randomizer();
        int index = Random.Range(0, 5);
        
        HairBack.sprite = Data.HairData.Hairs[index].Back;
        HairFront.sprite = Data.HairData.Hairs[index].Front;
    }

    public override void Activate()
    {
        base.Activate();
        TutorSpriteRenderers[0].sprite = HairBack.sprite;
        TutorSpriteRenderers[1].sprite = HairFront.sprite;
        ActivateTutor();
    }
}
