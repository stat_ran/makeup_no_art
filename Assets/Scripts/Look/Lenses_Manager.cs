﻿using UnityEngine;

public class Lenses_Manager : Manager
{
    public SpriteRenderer[] Lenses;
    
    public override void Start()
    {
        base.Start();
        IsSpawnTools = true;
        DeltaLeftPosition = 0.055f;
        foreach (var lens in Data.LensData.Lenses)
        {
            GameObject obj = Instantiate(Data.LensData.Prefab, DownMenu.ParentTools);
            obj.transform.localPosition=Vector3.zero;
            obj.GetComponent<LensesTool>().Init(Lenses, lens.Sprite, lens.LockNumber);
            Tools.Add(obj.GetComponent<Tool>());
        }
    }

    public override void Randomizer()
    {
        base.Randomizer();
        int index = Random.Range(0, 5);
        foreach (SpriteRenderer sr in Lenses)
            sr.sprite = Data.LensData.Lenses[index].Sprite;
    }
    
    public override void Activate()
    {
        base.Activate();
        ActivateTutor();
    }
    
}
