﻿using UnityEngine;

public class Skin_Manager : Manager
{

    [Header("Skins Settings")]
    public SpriteRenderer Skin;

    public SpriteRenderer EyelidL;
    public SpriteRenderer EyelidR;


    public override void Start()
    {
        base.Start();
        foreach (var tool in Tools)
        {
            ((SkinTool) tool).Skin = Skin;
            tool.Manager = this;
        }
        
    }

    public override void Randomizer()
    {
        base.Randomizer();
        int ran = Random.Range(0, Data.SkinData.SkinEyelids.Length);
        Skin.sprite = Data.SkinData.SkinEyelids[ran].Skin;
        ChangeEyelid(Data.SkinData.SkinEyelids[ran].Eyelid);
    }

    public void ChangeEyelid(Sprite sprite)
    {
        EyelidL.sprite=sprite;
        EyelidR.sprite=sprite;
    }
    
    public override void Activate()
    {
        base.Activate();
        ActivateTutor();
    }

}
