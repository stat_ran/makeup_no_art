﻿using UnityEngine;
using UnityEngine.UI;

public class SkinTool : Tool
{
    [HideInInspector] public SpriteRenderer Skin;

    public override void Start()
    {
        base.Start();
        IsEraser = false;
        IsMovementAllowed = false;
    }

    public override void Activate()
    {
        DownMenu.StartAnimArrow();
        for (int i = 0; i < GM.Data.SkinData.SkinEyelids.Length; i++)
        {
            if (GM.Data.SkinData.SkinEyelids[i].Skin == Skin.sprite)
            {
                int k = i + 1;
                if (k <= GM.Data.SkinData.SkinEyelids.Length - 1)
                {
                    Skin.sprite = GM.Data.SkinData.SkinEyelids[k].Skin;
                    (Manager as Skin_Manager)?.ChangeEyelid(GM.Data.SkinData.SkinEyelids[k].Eyelid);
                }
                else
                {
                    Skin.sprite = GM.Data.SkinData.SkinEyelids[0].Skin;
                    (Manager as Skin_Manager)?.ChangeEyelid(GM.Data.SkinData.SkinEyelids[0].Eyelid);
                }

                return;
            }
        }
    }
}
