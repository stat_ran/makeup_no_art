using System.Collections;
using System.Collections.Generic;
using AceCream.Scripts.Notification;
using UnityEngine;

public class Grim_Tool : Tool, IDrawTool
{
    
    [HideInInspector] public Sprite Sprite;

    private SpriteRenderer _spriteRenderer;

    public override void Start()
    {
        base.Start();
        NotificationCenter.addCallback(Notifications.CheckBuy, CheckBuy);
    }
    
    private void OnDestroy()
    {
        NotificationCenter.removeCallbeck(Notifications.CheckBuy, CheckBuy);
    }

    public void Init(Manager manager, Sprite sprite, Sprite icon, int lockNumber)
    {
        MyAwake();
        base.Init(manager);
        ManagersEnum = ManagersEnum.Grim;
        IsEraser = true;
        IconTool.sprite = icon;
        Sprite = sprite;
        LockNumber = lockNumber;
        DrawTool = this;
        CheckBuy(null);
    }

    public override void Activate()
    {
        if (IsLock)
        {
            MyShowRewardAds.Instance.Activate(GetName(), this);
            return;
        }
        base.Activate();
        Manager.TutorSpriteRenderers[0].sprite = Sprite;
        Manager.ActivateTutor();
    }
    
    public void Draw()
    {
        foreach (var targetImage in TargetImages)
        {
            Paint.DrawImage(targetImage, Sprite);
        }
       
    }
}
