﻿using UnityEngine;

public class Grim_Manager : Manager
{

    public override void Start()
    {
        base.Start();
        DeltaLeftPosition = 0.015f;
        IsSpawnTools = true;
        foreach (var grim in Data.GrimData.Grims)
        {
            GameObject obj = Instantiate(Data.GrimData.Prefab, DownMenu.ParentTools);
            obj.transform.localPosition=Vector3.zero;
            obj.GetComponent<Grim_Tool>().Init(this, grim.Sprite, grim.Icon, grim.LockNumber);
            Tools.Add(obj.GetComponent<Tool>());
        }
        
        Clear();
    }
}
