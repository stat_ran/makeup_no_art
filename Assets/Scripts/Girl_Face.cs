﻿using UnityEngine;

public class Girl_Face : InputObject
{
    private GameManager _gm;

    public override void Start()
    {
        base.Start();

        IsMovementAllowed = false;

        _gm = GameManager.Instance;
    }

    public override void TouchDown()
    {
        _gm.ScreenAction(TouchState.Down, this);
    }

    public override void TouchDrag()
    {
        _gm.ScreenAction(TouchState.Drag, this);
    }

    public override void TouchUp()
    {
        _gm.ScreenAction(TouchState.Up, this);
    }
}
