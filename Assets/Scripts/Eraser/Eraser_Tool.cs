using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Eraser_Tool : Tool
{
    public Sprite CursorTool;
    private int EraseRadius = 40;

    //[HideInInspector] public bool IsCrystal;

    private bool _isReady;
    private float _speedEr = 30f;
    
    
    public override void Start()
    {
        base.Start();
        _isReady = true;
    }

    public override void Activate()
    {
        base.Activate();
        GameManager.Instance.Winking.StopWatch();
        _isReady = false;
        DownMenu.IsClear = !DownMenu.IsClear;
        DownMenu.DownCuvettePanel();
    }

    public override void Deactivate()
    {
        base.Deactivate();
        DownMenu.IsClear = false;
    }

    private void Update()
    {
        if(!DownMenu.IsClear) return;
        
        
        if (Input.GetMouseButtonDown(0))
        {
            if (PointerRaycast(MousePosition))
            {
                if (!DownMenu.CursorTool.activeSelf)
                {
                    //Активируем префаб для курсора и ставим в начальную точку
                    DownMenu.CursorTool.SetActive(true);
                    DownMenu.CursorTool.transform.position = transform.position;
                    DownMenu.CursorTool.GetComponentInChildren<SpriteRenderer>().sprite = CursorTool;
                }
            }
        }
        
        if (Input.GetMouseButton(0)) 
        {
            if (PointerRaycast(MousePosition))
            {
                if (DownMenu.Manager && DownMenu.Manager.Tool)
                {
                    GameManager.Instance.Winking.SwitchWink(true);
                    if (!(DownMenu.Manager.Tool is CrystalTool))
                    {
                        //Чистим спрайты
                        foreach (var targetImage in DownMenu.Manager.Tool.TargetImages)
                        {
                            GM.Paint.EraseImage(targetImage, MousePosition, EraseRadius);
                        }
                    }
                    else
                    {
                        //Чистим кристалы
                        foreach (var hit in Physics2D.CircleCastAll(MousePosition, 0.3f, Vector2.zero))
                        {
                            if (hit.collider.name == "Crystal") Destroy(hit.collider.gameObject);
                        }
                    }
                }
                if (DownMenu.CursorTool.activeSelf)
                {
                    DownMenu.CursorTool.transform.DOKill();
                    Vector2 direct = MousePosition - (Vector2)DownMenu.CursorTool.transform.position;
                    DownMenu.CursorTool.transform.position = Vector2.Lerp(DownMenu.CursorTool.transform.position,
                        MousePosition, Time.deltaTime * _speedEr);
                }
            }
            else
            {
                if (DownMenu.CursorTool.activeSelf)
                {
                    DownMenu.CursorTool.SetActive(false);
                }
            }
        }
        

        if (Input.GetMouseButtonUp(0))
        {
            if (PointerRaycast(MousePosition))
            {
                if (DownMenu.CursorTool.activeSelf)
                {
                    DownMenu.CursorTool.transform.DOMove(transform.position, 0.2f).SetEase(Ease.Linear).onComplete +=
                        HideCursorTool;
                }
            }
            GameManager.Instance.Winking.SwitchWink(false);
        }
    }
    
    bool PointerRaycast(Vector2 position)
    {
        PointerEventData pointerData = new PointerEventData(EventSystem.current);
        pointerData.position = position;
       
#if !UNITY_EDITOR
		if(EventSystem.current.IsPointerOverGameObject(0)) return false;
		else return true;
#endif
        
#if UNITY_EDITOR
        if (EventSystem.current.IsPointerOverGameObject()) return false;
        else return true;
#endif
    }
    
    private void HideCursorTool()
    {
        DownMenu.CursorTool.SetActive(false);
    }
    
}
