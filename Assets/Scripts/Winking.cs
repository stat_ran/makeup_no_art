﻿using System.Collections;
using UnityEngine;

public class Winking : MonoBehaviour
{
    public Transform[] EyeLenses = new Transform[2];
    public GameObject[] Eyelids = new GameObject[2];
    public SpriteRenderer[] Shadows = new SpriteRenderer[2];
    public Vector2 Probability = new Vector2(5f, 10f);

    public bool IsWinkAllowed
    {
        get => _isWinkAllowed;
        set
        {
            _isWinkAllowed = value;
            _curTime = 0;
        }
    }

    private Vector2[] _lensesPos;
    private bool _isWinkAllowed;
    private float _curTime = 0;
    private float _curCooldown;
    
    void Start()
    {
        SwitchWink(false);
        _lensesPos = new Vector2[EyeLenses.Length];
        for (var i = 0; i < EyeLenses.Length; i++)
            _lensesPos[i] = EyeLenses[i].localPosition;
    }

    void Update()
    {
        return;
        _curTime += Time.deltaTime;
        if (_curTime > _curCooldown && _isWinkAllowed)
        {
            _curTime = 0;
            _curCooldown = Random.Range(Probability.x, Probability.y);
            StartCoroutine(Wink());
        }
    }

    public void EyeWatch(Vector2 point)
    {
        Vector2 dir = point - (_lensesPos[0] + _lensesPos[1]) / 2;
        for (var i = 0; i < EyeLenses.Length; i++)
            EyeLenses[i].localPosition = _lensesPos[i] + dir.normalized * Mathf.Sqrt(dir.magnitude/100);
    }

    public void StopWatch()
    {
        for (var i = 0; i < EyeLenses.Length; i++)
            EyeLenses[i].localPosition = _lensesPos[i];
    }

    public void SwitchWink(bool isWink)
    {
        foreach (GameObject eyelid in Eyelids)
            eyelid.SetActive(isWink);
        foreach (SpriteRenderer shadow in Shadows)
            shadow.maskInteraction = isWink ? SpriteMaskInteraction.None : SpriteMaskInteraction.VisibleInsideMask;
    }

    IEnumerator Wink()
    {
        SwitchWink(true);
        yield return new WaitForSeconds(0.2f);
        if (_isWinkAllowed)
            SwitchWink(false);
    }
    
}
