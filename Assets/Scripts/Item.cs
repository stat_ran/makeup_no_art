﻿using AceCream.Scripts.Notification;
using UnityEngine;
using UnityEngine.UI;

public class Item : BaseBahavior
{
    [HideInInspector] public Tool Tool;
    [HideInInspector] public Sprite Sprite;
    [HideInInspector] public Color Color;
    [HideInInspector] public GameObject Prefab;
    [HideInInspector] public Sprite Stick; //Только для помады
    [HideInInspector] public int LockNumber;
    [HideInInspector] public Image LockImage;
    [HideInInspector] public ManagersEnum ManagersEnum;
    [HideInInspector] public RectTransform RectTransform;

    private Image _outline;
    private Image _icon;
    
    [Header("Lock")]
    public string NameLock;
    public bool IsLock;
    [Space]
    [HideInInspector] public GameObject LockObj;

    public override void Awake()
    {
        base.Awake();
        _outline = GetComponent<Image>();
        _outline.enabled = true;
        Outline(false);
        if(transform.childCount>0) _icon = transform.GetChild(0).GetComponent<Image>();
        if (transform.childCount > 1) LockImage = transform.GetChild(1).GetComponent<Image>();
        RectTransform = GetComponent<RectTransform>();
        GetComponent<Button>()?.onClick.AddListener(Activate);
    }

    public override void Start()
    {
        base.Start();
        CheckBuy(null);
        NotificationCenter.addCallback(Notifications.CheckBuy, CheckBuy);
    }

    private void OnDestroy()
    {
        NotificationCenter.removeCallbeck(Notifications.CheckBuy, CheckBuy);
    }

    public virtual void Activate()
    {
        DownMenu.StopAnimArrow();
        if (IsLock)
        {
            MyShowRewardAds.Instance.Activate(GetName(), this);
            return;
        }
        if (Tool.SelectedItem == this)
        {
            DownMenu.Eraser.Back();
            return;
        }
        if(Tool.SelectedItem) Tool.SelectedItem.DeActivate();
        Tool.SetItem(this);
        Outline(true);
        DownMenu.Eraser.Back();
        transform.localScale=Vector3.one*1.2f;
    }

    public virtual void DeActivate()
    {
        Outline(false);
        transform.localScale=Vector3.one;
    }

    private void Outline(bool isActive)
    {
        _outline.color = isActive ? Color.white : GM.ColorOutlineCuvete;
    }

    public void Init(Tool tool, Sprite icon)
    {
        if (!_icon) _icon = GetComponent<Image>();
        Tool = tool;
        _icon.sprite = icon;
        gameObject.SetActive(false);
    }

    public void SetSprite(Sprite sprite)
    {
        Sprite = sprite;
    }

    public void SetColor(Color color)
    {
        Color = color;
    }

    public void SetPrefab(GameObject prefab)
    {
        Prefab = prefab;
    }

    public void SetStick(Sprite sprite)
    {
        Stick = sprite;
    }

    public void SetLockNumber(int lockNumber)
    {
        LockNumber = lockNumber;
        if (transform.childCount > 1) LockImage = transform.GetChild(1).GetComponent<Image>();
    }

    public void SetType(ManagersEnum managersEnum)
    {
        ManagersEnum = managersEnum;
    }

    public void CheckBuy(object obj)
    {
        if (PlayerPrefs.HasKey(GetName()) || MySDK.Instance.NoAds || LockNumber==0)
        {
            LockImage.enabled = false;
            IsLock = false;
        }
        else
        {
            LockImage.enabled = true;
            IsLock = true;
        }

        
        if (obj is Item tool && tool == this)
        {
            Activate();
        }
    }
    
    public string GetName()
    {
        return "Lock" + ManagersEnum + LockNumber;
    }
}
