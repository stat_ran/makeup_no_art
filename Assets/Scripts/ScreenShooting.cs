﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;

public class ScreenShooting : MonoBehaviour
{
    private static ScreenShooting _instance;
    
    public static ScreenShooting Instance
    {
        get    
        {
            if(_instance == null)
                _instance = FindObjectOfType<ScreenShooting>();
            return _instance;
        }
    }

    /// <summary>
    /// Captures a screenshot at path fileName from camera's Render Texture. Returns file path.
    /// </summary>
    /// <param name="fileName">Name of created image in file system</param>
    /// <param name="camera">Target Camera to take screenshot</param>
    public string TakePhoto(string fileName = "Screenshot.png", Camera camera = null)
    {
        if (camera == null) camera = Camera.main;
        if (camera == null) return null;

        Texture2D image = RTImage(camera);
        byte[] bytes = image.EncodeToPNG();

    #if !UNITY_EDITOR
        string filePath = Application.persistentDataPath + "/" + fileName;
    #else
        string filePath = new FileInfo(fileName).Directory + "/" + fileName;
    #endif

        File.WriteAllBytes(filePath, bytes);

        return filePath;
    }

    /// <summary>
    /// Captures a screenshot at path fileName from camera's Render Texture and calls action when saving is completed. Returns file path.
    /// </summary>
    /// <param name="fileName">Name of created image in file system</param>
    /// <param name="camera">Target Camera to take screenshot</param>
    /// <param name="onDone">Action that called when saving is completed</param>
    public string TakePhoto(string fileName, Camera camera, Action onDone)
    {
        string filePath = TakePhoto(fileName, camera);

        if (filePath == null) return null;

        if (onDone != null)
            StartCoroutine(CaptureScreen(filePath, onDone));

        return filePath;
    }

    /// <summary>
    /// Captures a screenshot at path fileName from camera's Render Texture and calls action when saving is completed with filePath as input param. Returns file path.
    /// </summary>
    /// <param name="fileName">Name of created image in file system</param>
    /// <param name="camera">Target Camera to take screenshot</param>
    /// <param name="onDone">Action that called when saving is completed</param>
    public string TakePhoto(string fileName, Camera camera, Action<string> onDone)
    {
        string filePath = TakePhoto(fileName, camera);

        if (filePath == null) return null;

        if (onDone != null)
            StartCoroutine(CaptureScreen(filePath, onDone));

        return filePath;
    }

    /// <summary>
    /// Returns a texture from camera's Render Texture.
    /// </summary>
    /// <param name="camera">Target Camera to take screenshot</param>
    public static Texture2D RTImage(Camera camera)
    {
        var size = new Vector2Int((int)Screen.width, (int)Screen.height);
        var cameraTargetTexture = new RenderTexture(size.x, size.y, 24);
        camera.targetTexture = cameraTargetTexture;
        RenderTexture.active = camera.targetTexture;
        camera.Render();
        var image = new Texture2D(size.x, size.y);
        image.ReadPixels(new Rect(0, 0, size.x, size.y), 0, 0);
        image.Apply();
        RenderTexture.active = null;
        camera.targetTexture = null;

        return image;
    }

    /// <summary>
    /// Captures a screenshot at path fileName with multiplier and calls action when saving is completed. Returns path to file.
    /// </summary>
    /// <param name="fileName">Name of created image in file system</param>
    /// <param name="sizeMultiplier">Multiplier for increasing resolution</param>
    /// <param name="onDone">Action that called when saving is completed</param>
    public string CaptureScreenshot(string fileName, int sizeMultiplier, Action onDone)
    {
        string filePath = CaptureScreenshot(fileName, sizeMultiplier);
        
        if (onDone != null)
            StartCoroutine(CaptureScreen(filePath, onDone));
        
        return filePath;
    }

    /// <summary>
    /// Captures a screenshot at path fileName with multiplier and calls action when saving is completed with filePath as input param. Returns path to file.
    /// </summary>
    /// <param name="fileName">Name of created image in file system</param>
    /// <param name="sizeMultiplier">Multiplier for increasing resolution</param>
    /// <param name="onDone">Action that called when saving is completed</param>
    public string CaptureScreenshot(string fileName, int sizeMultiplier, Action<string> onDone)
    {
        string filePath = CaptureScreenshot(fileName, sizeMultiplier);

        if (onDone != null)
            StartCoroutine(CaptureScreen(filePath, onDone));

        return filePath;
    }

    /// <summary>
    /// Captures a screenshot at path fileName with multiplier. Returns path to file.
    /// </summary>
    /// <param name="fileName">Name of created image in file system</param>
    /// <param name="sizeMultiplier">Multiplier for increasing resolution</param>
    /// <param name="onDone">Action that called when saving is completed</param>
    public string CaptureScreenshot(string fileName = "Screenshot.png", int sizeMultiplier = 1)
    {
        if (!fileName.EndsWith(".png", StringComparison.OrdinalIgnoreCase)) fileName += ".png";
        ScreenCapture.CaptureScreenshot(fileName, sizeMultiplier);
        
    #if !UNITY_EDITOR
        string filePath = Application.persistentDataPath + "/" + fileName;
    #else
        string filePath = new FileInfo(fileName).Directory + "/" + fileName;
    #endif

        return filePath;
    }
    
    private IEnumerator CaptureScreen(string filePath, Action onDone)
    {
        if (!File.Exists(filePath)) yield break;
        yield return new WaitWhile(() => IsFileLocked(new FileInfo(filePath)));
        
        onDone?.Invoke();
    }
    
    private IEnumerator CaptureScreen(string filePath, Action<string> onDone)
    {
        if (!File.Exists(filePath)) yield break;
        yield return new WaitWhile(() => IsFileLocked(new FileInfo(filePath)));
        
        onDone?.Invoke(filePath);
    }
    
    /// <summary>
    /// Returns true if file is: 1) still being written to;
    /// 2) or being processed by another thread;
    /// 3) or does not exist (has already been processed).
    /// </summary>
    public static bool IsFileLocked(FileInfo fileInfo)
    {
        try
        {
            using(FileStream stream = fileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.None))
            {
                stream.Close();
            }
        }
        catch (IOException)
        {
            return true;
        }

        return false;
    }
    
    /// <summary>
    /// Load a PNG or JPG file from disk to a Texture2D. Returns null if load fails
    /// </summary>
    public static Texture2D LoadTexture(string filePath)
    {
        if (File.Exists(filePath)){
            var fileData = File.ReadAllBytes(filePath);
            var tex2D = new Texture2D(2, 2);
            if (tex2D.LoadImage(fileData))
                return tex2D;
        }  
        return null;
    }
    
    /// <summary>
    /// Load a PNG or JPG image from disk to a Texture2D, assign this texture to a new sprite and return its reference
    /// </summary>
    public static Sprite LoadNewSprite(string filePath, float pixelsPerUnit = 100.0f)
    {
        Texture2D spriteTexture = LoadTexture(filePath);
        var newSprite = Sprite.Create(spriteTexture, new Rect(0, 0, spriteTexture.width, spriteTexture.height),Vector2.one * 0.5f, pixelsPerUnit);
 
        return newSprite;
    }
}
