﻿using UnityEngine;

public enum AnchorHorizontal { Left, Center, Right, Custom }
public enum AnchorVertical { Bottom, Middle, Top, Custom }

public class AnchorSprite : MonoBehaviour
{
    public bool IsValidate = false;
    [Space]
    public Camera Camera;
    public Vector2 Anchors;
    public Vector2 Offset;

    [Header("Applied Anchors")]
    public AnchorHorizontal AnchorHorizontal;
    public AnchorVertical AnchorVertical;

    [Header("Scaling")]
    public bool IsScalable;
    public float ReferencedResolution;

    private Rect _camRect;
    private AnchorHorizontal _hor;
    private AnchorVertical _vert;

#if UNITY_EDITOR
    public void OnValidate()
    {
        if (Application.isPlaying || !IsValidate) return;

        if (IsScalable && ReferencedResolution <= 0.01f)
            ReferencedResolution = (float)Screen.height / Screen.width;
        
        if (!Camera)
            if (Camera.main)
                Camera = Camera.main;

        Anchors = new Vector2(Mathf.Clamp01(Anchors.x), Mathf.Clamp01(Anchors.y));
        if (AnchorHorizontal != _hor || AnchorVertical != _vert)
        {
            switch (AnchorHorizontal)
            {
                case AnchorHorizontal.Left:
                    Anchors.x = 0;
                    break;
                case AnchorHorizontal.Center:
                    Anchors.x = 0.5f;
                    break;
                case AnchorHorizontal.Right:
                    Anchors.x = 1;
                    break;
            }
            switch (AnchorVertical)
            {
                case AnchorVertical.Bottom:
                    Anchors.y = 0;
                    break;
                case AnchorVertical.Middle:
                    Anchors.y = 0.5f;
                    break;
                case AnchorVertical.Top:
                    Anchors.y = 1;
                    break;
            }
        }
        else
        {
            switch (Anchors.x)
            {
                case 0:
                    AnchorHorizontal = AnchorHorizontal.Left;
                    break;
                case 0.5f:
                    AnchorHorizontal = AnchorHorizontal.Center;
                    break;
                case 1:
                    AnchorHorizontal = AnchorHorizontal.Right;
                    break;
                default:
                    AnchorHorizontal = AnchorHorizontal.Custom;
                    break;
            }

            switch (Anchors.y)
            {
                case 0:
                    AnchorVertical = AnchorVertical.Bottom;
                    break;
                case 0.5f:
                    AnchorVertical = AnchorVertical.Middle;
                    break;
                case 1:
                    AnchorVertical = AnchorVertical.Top;
                    break;
                default:
                    AnchorVertical = AnchorVertical.Custom;
                    break;
            }
        }
        _hor = AnchorHorizontal;
        _vert = AnchorVertical;

        if (Camera)
        {
            _camRect = new Rect(Camera.ViewportToWorldPoint(new Vector2(0, 0)), Camera.ViewportToWorldPoint(new Vector2(1, 1)) * 2);
            Offset = _camRect.position + _camRect.size * Anchors - (Vector2)transform.position;
        }
    }

    [ContextMenu("Validate All")]
    private void ValidateAll()
    {
        foreach (AnchorSprite item in Resources.FindObjectsOfTypeAll(GetType()))
        {
            item.OnValidate();
        }
    }
#endif

    private void Awake()
    {
        if (Camera)
            _camRect = new Rect(Camera.ViewportToWorldPoint(new Vector2(0, 0)), Camera.ViewportToWorldPoint(new Vector2(1, 1)) * 2);

        Vector2 newPos = _camRect.position + _camRect.size * Anchors - Offset;
        var t = transform;
        t.position = new Vector3(newPos.x, newPos.y, t.position.z);
        if (IsScalable)
        {
            float res = (float)Screen.height / Screen.width;
            t.localScale *= ReferencedResolution / res;
        }
    }
}
