﻿using AceCream.Scripts.Notification;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class StartMenu : BaseBahavior
{

    public static StartMenu Instance;

    public Button b_Play;
    public Button b_Album;
    
    [Header("Change Start Girl")]
    public Sprite[] Girls;
    public Image StartGirl;
    public Image ChildGirl;
    public float Cooldown = 10f;


    public override void Awake()
    {
        base.Awake();
        if (Instance == null) Instance = this;
        b_Album.onClick.AddListener(C_Album);
        b_Play.onClick.AddListener(StartGame);
    }

    void Start()
    {
        Activate(true);
    }

    void Update()
    {
        if(GameManager.Instance.IsGamePlay) return;
        if ((int)(Time.time % Cooldown) == 0)
            if ((int)(Time.time % Cooldown) != (int)((Time.time - Time.deltaTime) % Cooldown))
                ChangeGirl();
    }
    
    public void Activate(bool isActive)
    {
        gameObject.SetActive(isActive);
        MyAds.Instance.HideBanner();
    }

    public void StartGame()
    {
        GameManager.Instance.IsGamePlay = true;
        Activate(false);
        GM.Init();
        DownMenu.Activate();
        Photo_Manager.Instance.Deactivate();
        
        MyAds.Instance.ShowBanner();
        MyMetrica.Instance.ReportStartLevel(1, StartType.new_game);
        MySDK.Instance.IsContinueGame = false;
            
        //NotificationCenter.postNotification(Notifications.OpenStore, null);
    }

    private void ChangeGirl()
    {
        int i = 0;
        for (; i < Girls.Length; i++)
            if (StartGirl.sprite == Girls[i])
                break;
        ChildGirl.sprite = StartGirl.sprite;
        ChildGirl.color = Color.white;
        ChildGirl.DOFade(0, 1).SetEase(Ease.Linear);

        StartGirl.sprite = Girls[(i + 1) % Girls.Length];
        StartGirl.color = new Color(1, 1, 1, 0);
        StartGirl.DOFade(1, 0.8f).SetEase(Ease.Linear);
    }

    private void C_Album()
    {
        Photo_Album.Instance.Activate();
    }
}
