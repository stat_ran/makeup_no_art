﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIBar : MonoBehaviour
{

    public static UIBar Instance;
    
    public Transform Selector;
    public Button ArrowR;
    public Button ArrowL;
    public Image[] Icons;

    private GameManager GM;
    private int _currentLevel;
    private float _time;

    [HideInInspector] public float[] timers=new float[8];
    [HideInInspector] public bool IsLevel;
    private Tutorial _tutorial;

    private void Awake()
    {
        _currentLevel = -1;
        if(Instance==null) Instance = this;
        _tutorial = FindObjectOfType<Tutorial>();
    }

    private void Start()
    {
        GM = GameManager.Instance;
    }

    public void MoveSelector(ManagersEnum managers)
    {
        Selector.DOKill();
        Selector.DOMoveX(Icons[(int)managers].transform.position.x, 0.2f).SetEase(Ease.Linear);
    }

    public void StartLevel(int n)
    {
        _currentLevel = n;
        _time = Time.time;
    }

    void FinishLevel(int level, float _time)
    {
        timers[level] += _time;
    }


    private void OnApplicationPause(bool pauseStatus)
    {
        if( !IsLevel ) return;
        if (pauseStatus)
        {
            if(_currentLevel>=0 || _currentLevel<8) FinishLevel(_currentLevel, Time.time - _time);
            if (MySDK.Instance.IsContinueGame)
                MyMetrica.Instance.ReportFinishLevel(1, StartType.continue_game, timers, 23f,
                    Result.minimize);
            else
            {
                MyMetrica.Instance.ReportFinishLevel(1, StartType.new_game, timers, 23f,
                    Result.minimize);
            }
        }
        else
        {
            MyMetrica.Instance.ReportStartLevel(1, StartType.continue_game);
            MySDK.Instance.IsContinueGame = true;
        }
    }
    
}
