﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using AceCream.Scripts.Notification;
using DG.Tweening;

public class UITopMenu : MonoBehaviour
{
    [Header("Photo")]
    public GameObject SharePanel;
    public Image ScreenshotImage;
    public Camera TargetCamera;
    public AudioSource AudioSource;
    public AudioClip Photo;
    public AudioClip Paparatci;
    public Transform Flash;
    
    private GameManager GM;
    private Vector2 _gridSize;
    [HideInInspector] public bool IsActive = false;
    private float _moveDuration = 0.15f;
    private bool _isMusicEnabled = true;
    private Texture2D _screenshot;
    

    public static UITopMenu Instance;

    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        GM = GameManager.Instance;

        //_screenshot = new Texture2D(TargetCamera.targetTexture.width, Mathf.RoundToInt(TargetCamera.targetTexture.height * (1 - TargetCamera.rect.y)));
    }

    public void Activate()
    {
        IsActive = !IsActive;

        if (IsActive)
        {
            MyAds.Instance.HideBanner();
        }
        else
        {
            MyAds.Instance.ShowBanner();
        }
    }

    public void OpenPanel(GameObject panel)
    {
        panel.SetActive(true);
    }

    public void GoToStartMenu(GameObject panel)
    {
        panel.SetActive(false);
        GM.DeInit();
        GM.StartMenu.Activate(true);
        if (MySDK.Instance.IsContinueGame)
            MyMetrica.Instance.ReportFinishLevel(1, StartType.continue_game, UIBar.Instance.timers, 23f,
                Result.leave);
        else
            MyMetrica.Instance.ReportFinishLevel(1, StartType.new_game, UIBar.Instance.timers, 23f,
                Result.leave);
        UIBar.Instance.IsLevel = false;
        GameManager.Instance.IsGamePlay = false;
    }

    public void ResetGirl(GameObject panel)
    {
        GM.GetGift();
        panel.SetActive(false);
        GM.TotalClear();
        //GM.SetSection(ManagersEnum.Skin);
        if (MySDK.Instance.IsContinueGame)
            MyMetrica.Instance.ReportFinishLevel(1, StartType.continue_game, UIBar.Instance.timers, 23f,
                Result.reset);
        else
            MyMetrica.Instance.ReportFinishLevel(1, StartType.new_game, UIBar.Instance.timers, 23f,
                Result.reset);
        MyMetrica.Instance.ReportStartLevel(1, StartType.new_game);
        MySDK.Instance.IsContinueGame = false;
        for (int i = 0; i < 8; i++)
        {
            UIBar.Instance.timers[i] = 0;
        }

    }

    public void PhotoButton()
    {

        foreach (var spriteRenderer in GM.TutorSpriteRenderers)
        {
            spriteRenderer.color=Color.clear;
        }
        Flash.localScale=Vector3.one;
        Flash.GetComponent<Image>().enabled=true;
        Flash.DOScale(Vector3.one * 50f, 0.1f).SetEase(Ease.Linear).onComplete+=BackFlash;

        var size = new Vector2Int(TargetCamera.targetTexture.width, TargetCamera.targetTexture.height);
        RenderTexture.active = TargetCamera.targetTexture;
        TargetCamera.Render();

        var image = new Texture2D(size.x, size.y);
        image.ReadPixels(new Rect(0, 0, size.x, size.y), 0, 0);
        Debug.Log(_screenshot);
        _screenshot.SetPixels(image.GetPixels(0, size.y - _screenshot.height, _screenshot.width, _screenshot.height));
        _screenshot.Apply();

        RenderTexture.active = null;

        ScreenshotImage.sprite = Sprite.Create(_screenshot, new Rect(0, 0, _screenshot.width, _screenshot.height), Vector2.one * 0.5f);
        
        SharePanel.SetActive(true);
        if (_isMusicEnabled)
        {
            AudioSource.PlayOneShot(Photo);
            AudioSource.PlayOneShot(Paparatci);
        }
    }

    private void BackFlash()
    {
        Flash.DOScale(Vector3.one, 0.1f).SetEase(Ease.Linear).onComplete += OffFlash;
    }

    private void OffFlash()
    {
        Flash.GetComponent<Image>().enabled=false;
    }

    public void SharePhoto()
    {
        SharePanel.SetActive(false);
        ScreenshotImage.sprite = null;
        new NativeShare().AddFile(_screenshot)
            .SetTitle("Screenshot")
            .Share();
    }

    public void Shop()
    {
        NotificationCenter.postNotification(Notifications.OpenStore, null);
    }
}
