using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointBar : MonoBehaviour
{
   public GameObject PointPrefab;

   private List<PointPrefab> _pointPrefabs=new List<PointPrefab>();
   private DownMenu _downMenu;

   public void Init(int countPoint)
   {
      _downMenu = GetComponentInParent<DownMenu>();
      for (int i = 0; i < countPoint; i++)
      {
         PointPrefab pointPrefab = Instantiate(PointPrefab, transform).GetComponent<PointPrefab>();
         _pointPrefabs.Add(pointPrefab);
         pointPrefab.Init();
      }
      SetOutlinePoint(_downMenu.GM.CurrentTool);
   }

   public void SetOutlinePoint(int numberPoint)
   {
      foreach (var prefab in _pointPrefabs)
      {
         prefab.OffOutline();
      }
      _pointPrefabs[numberPoint].OnOutline();
   }

}
