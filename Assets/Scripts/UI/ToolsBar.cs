using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolsBar : MonoBehaviour
{

    public Transform Left;
    public Transform Right;
    public Transform Up;
    public Transform Down;
    
    private DownMenu _downMenu;

    public void Init()
    {
        _downMenu = GetComponentInParent<DownMenu>();
    }
}
