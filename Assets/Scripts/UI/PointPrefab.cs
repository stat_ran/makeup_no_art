using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointPrefab : MonoBehaviour
{
    private Image Outline;

    public void Init()
    {
        Outline = transform.GetChild(0).GetComponent<Image>();
        OffOutline();
    }

    public void OffOutline()
    {
        Outline.enabled = false;
    }

    public void OnOutline()
    {
        Outline.enabled = true;
    }
}
