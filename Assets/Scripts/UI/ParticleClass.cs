using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleClass : MonoBehaviour
{

    private ParticleSystem _particle;
    private AudioSource _audio;
    private Transform _transform;
    
    void Start()
    {
        _particle = GetComponent<ParticleSystem>();
        _audio = GetComponent<AudioSource>();
        _transform = transform;
    }

    public void Play(Vector2 pos)
    {
        _transform.position = pos;
        _particle.Play();
        _audio.PlayOneShot(_audio.clip);
    }

}
