using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class GiftPanel : MonoBehaviour
{

   public Transform PointItem;

   private Item _item;
   
   private void Start()
   {
      gameObject.SetActive(false);
   }

   public void Open(Item item)
   {
      gameObject.SetActive(true);
      GameObject obj = Instantiate(item.gameObject, PointItem);
      obj.transform.position = PointItem.position;
      _item = obj.GetComponent<Item>();
      _item.LockObj.transform.eulerAngles=new Vector3(0,0,10f);
      _item.LockObj.transform.DORotate(Vector3.forward * -10, 0.2f).SetLoops(5, LoopType.Yoyo).SetEase(Ease.Linear)
         .onComplete += ScaleItem;
      _item.LockObj.SetActive(true);
      _item.GetComponent<SpriteRenderer>().sortingLayerName = "UI_Elements";
      _item.GetComponent<SpriteRenderer>().sortingOrder = 115;
      _item.LockObj.GetComponent<SpriteRenderer>().sortingOrder = 120;
   }

   private void ScaleItem()
   {
      _item.LockObj.transform.DOScale(_item.LockObj.transform.localScale * 1.2f, 1f).SetEase(Ease.Linear).onComplete +=
         EndParticle;
   }

   private void EndParticle()
   {
      _item.LockObj.SetActive(false);
      GameManager.Instance.ParticleStars.Play(_item.LockObj.transform.position);
      StartCoroutine(Timer());
   }

   IEnumerator Timer()
   {
      yield return new WaitForSeconds(2f);
      Destroy(_item.gameObject);
      gameObject.SetActive(false);
   }

}
