using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DownMenu : BaseBahavior
{
    public static DownMenu Instance;
    
    public Button ArrowLeft;
    public Button ArrowRight;
    public Button Photo;
    public Transform ParentTools;
    public Transform ContainerTools;
    public Transform ParentCuvet;
    public Transform ContainerCuvet;
    public Transform ToolsPanel;
    public Transform CuvetPanel;
    public Transform ArrowDown;
    public GameObject CursorToolPrefab;
    public RectTransform FonAds;
    
    [HideInInspector] public PointBar PointBar;
    [HideInInspector] public ToolsBar ToolsBar;
    [HideInInspector] public CuveteBar CuveteBar;
    [HideInInspector] public Manager Manager;
    [HideInInspector] public Eraser Eraser;
    [HideInInspector] public bool IsClear;
    [HideInInspector] public GameObject CursorTool;

    private Managers _managers;
    [SerializeField] private bool _isMoveManager;
    private ScrollRect _scrollToolsPanel;
    private ScrollRect _scrollCuvettePanel;
    private Vector2 _startPosCuvettePanel;
    private Coroutine _coroutineTool;
    private Coroutine _coroutineCuvette;
    private Coroutine _coroutineArrow;
    private RectTransform _parentPanel;
    private RectTransform _cuvetPanel;

    private RectTransform _rectTransform;
    //[SerializeField] private float _value;

    public override void Awake()
    {
        base.Awake();
        if (Instance == null) Instance = this;
        PointBar = GetComponentInChildren<PointBar>(true);
        ToolsBar = GetComponentInChildren<ToolsBar>(true);
        CuveteBar = GetComponentInChildren<CuveteBar>(true);
        Eraser = GetComponentInChildren<Eraser>(true);
        Image[] images = ContainerTools.GetComponentsInChildren<Image>();
        foreach (var image in images)
        {
            Destroy(image.gameObject);
        }
        Image[] images2 = ContainerCuvet.GetComponentsInChildren<Image>();
        foreach (var image in images2)
        {
            Destroy(image.gameObject);
        }

        _scrollToolsPanel = ToolsPanel.GetComponentInChildren<ScrollRect>();
        _scrollCuvettePanel = CuvetPanel.GetComponentInChildren<ScrollRect>();
        _cuvetPanel = CuvetPanel.GetComponent<RectTransform>();
        _rectTransform = GetComponent<RectTransform>();
        _startPosCuvettePanel = CuvetPanel.transform.localPosition;
        CursorTool = Instantiate(CursorToolPrefab);
        CursorTool.SetActive(false);
        _parentPanel = transform.parent.GetComponent<RectTransform>();
        FonAds.gameObject.SetActive(false);
    }

    public void Init()
    {
        _managers = GM.Managers;
        PointBar.Init(GM.ManagersTool.Length);
        CuveteBar.Init();
        ToolsBar.Init();
        ArrowLeft.onClick.AddListener(LeftButton);
        ArrowRight.onClick.AddListener(RightButton);
        CheckArrow();
        StartManager();
    }

    public void BackToThis()
    {
        gameObject.SetActive(true);
        _isMoveManager = false;
    }

    private void StartManager()
    {
        //TODO Ставим первого менеджера на панель
        //Manager = GM.ManagersTool[0];
        //Manager.Activate();
        //InitFirstManager();
        GM.CurrentTool = 0;
        ChangeManager(GM.ManagersTool[0]);
        DownCuvettePanel();
    }

    private void CheckArrow()
    {
        if (GM.CurrentTool == 0)
        {
            ArrowLeft.gameObject.SetActive(false);
            ArrowRight.gameObject.SetActive(true);
            Photo.gameObject.SetActive(false);
            return;
        }

        if (GM.CurrentTool == GM.ManagersTool.Length - 1)
        {
            ArrowLeft.gameObject.SetActive(true);
            ArrowRight.gameObject.SetActive(false);
            Photo.gameObject.SetActive(true);
            return;
        }
        
        ArrowLeft.gameObject.SetActive(true);
        ArrowRight.gameObject.SetActive(true);
        Photo.gameObject.SetActive(false);
    }

    private void LeftButton()
    {
        if(_isMoveManager) return;
        GM.CurrentTool--;
        ChangeManager(GM.ManagersTool[GM.CurrentTool]);
        _isMoveManager = true;
        GameManager.Instance.Winking.SwitchWink(false);
        StopAnimArrow();
    }

    private void RightButton()
    {
        if(_isMoveManager) return;
        GM.CurrentTool++;
        ChangeManager(GM.ManagersTool[GM.CurrentTool]);
        _isMoveManager = true;
        GameManager.Instance.Winking.SwitchWink(false);
        StopAnimArrow();
    }

    private void ChangeManager(Manager manager)
    {
        PointBar.SetOutlinePoint(GM.CurrentTool);
        CheckArrow();
        
        SetManager(manager);
        
    }

    private void SetManager(Manager newManager)
    {
        MoveBackCurrentTools();
        if(Manager) Manager.Deactivate();
        Manager = newManager;
        Manager.Activate();
        DownMenu.CursorTool.transform.DOKill();
        DownMenu.CursorTool.SetActive(false);
        //if(_coroutineTool!=null) StopCoroutine(_coroutineTool);
        StopAllCoroutines();
        _coroutineTool = StartCoroutine(MoveNewTools());
        foreach (var tool in Manager.Tools)
        {
            if (tool.Outline.color==Color.white && tool.Items.Count>0) //tool.Outline.enabled && 
            {
                UpCuvettePanel(tool);
                return;
            }
        }
        DownCuvettePanel();
    }

    private void MoveBackCurrentTools()
    {
        if(!Manager) return;
        foreach (var tool in Manager.Tools)
        {
            tool.transform.parent = ParentTools;
            tool.transform.localPosition=Vector3.zero;
            tool.gameObject.SetActive(false);
        }
    }

    IEnumerator MoveNewTools()
    {
        float speed = 2f;
        float width = 0;
        float end = 0;
        foreach (var tool in Manager.Tools)
        {
            tool.gameObject.SetActive(true);
            tool.transform.parent = ContainerTools;
            if(tool.Outline.color==Color.white) UpCuvettePanel(tool);
            width += tool.RectTransform.rect.width;
        }

        width += (Manager.Tools.Count - 1) * 40;
        if (width < _rectTransform.rect.width + Manager.Tools[0].RectTransform.rect.width / 2f)
        {
            end = 1f;
        }
        else
        {
            end = (Manager.Tools[0].RectTransform.rect.width + 40) / 2f/(width - _rectTransform.rect.width);
        }

        _scrollToolsPanel.horizontalNormalizedPosition=1f;
        
        while (true)
        {
            _scrollToolsPanel.horizontalNormalizedPosition -= Time.deltaTime * speed;
            yield return null;
            if (_scrollToolsPanel.horizontalNormalizedPosition <= end)
            {
                _scrollToolsPanel.horizontalNormalizedPosition = end;
                _isMoveManager = false;
                GameManager.Instance.Winking.SwitchWink(false);
                foreach (var tool in Manager.Tools)
                {
                    if (tool.IsEraser)
                    {
                        Eraser.ShowEraser();
                        yield break;
                    }
                }
                yield break;
            }
        }
        yield break;
    }

    private void InitFirstManager()
    {
        ChangeManager(Manager);
        // foreach (var tool in Manager.Tools)
        // {
        //     tool.transform.parent = ContainerTools;
        // }
    }

    private void MoveBackCurrentCuvette()
    {
        foreach (var item in Manager.Tool.Items)
        {
            item.transform.parent = ParentCuvet;
            item.transform.localPosition=Vector3.zero;
        }
    }

    IEnumerator MoveNewCuvette()
    {
        if (!Manager.Tool) yield break;
        float speed = 2f;
        float width = 0;
        float end = 0;
        Item[] items= ContainerCuvet.GetComponentsInChildren<Item>();
        foreach (var item in items)
        {
            item.transform.parent = ParentCuvet;
            item.transform.localPosition=Vector3.zero;
            item.gameObject.SetActive(false);
        }
        foreach (var item in Manager.Tool.Items)
        {
            item.gameObject.SetActive(true);
            item.transform.parent = ContainerCuvet;
            width += item.RectTransform.rect.width;
        }
        
        width += (Manager.Tool.Items.Count - 1) * 40;
        if (width < _rectTransform.rect.width + Manager.Tool.Items[0].RectTransform.rect.width / 2f)
        {
            end = 1f;
        }
        else
        {
            end = (Manager.Tool.Items[0].RectTransform.rect.width + 40) / 2f/(width - _rectTransform.rect.width);
        }

        _scrollCuvettePanel.horizontalNormalizedPosition=1f;
        float delta = 0.1f/Manager.Tool.Items.Count;
        while (true)
        {
            _scrollCuvettePanel.horizontalNormalizedPosition -= Time.deltaTime * speed;
            yield return null;
            if (_scrollCuvettePanel.horizontalNormalizedPosition <= end)
            {
                _scrollCuvettePanel.horizontalNormalizedPosition = end;
                _isMoveManager = false;
                yield break;
            }
        }
        yield break;
    }

    public void UpCuvettePanel(Tool tool)
    {
        Manager.Tool = tool;
        if (CuvetPanel.localPosition.y < 0)
            CuvetPanel.DOLocalMove(_startPosCuvettePanel, 0.2f).SetEase(Ease.Linear).onComplete += EndUpCuvettePanel;
        else
        {
            EndUpCuvettePanel();
        }
        ArrowDown.gameObject.SetActive(true);
    }

    private void EndUpCuvettePanel()
    {
        //if(_coroutineCuvette!=null) StopCoroutine(_coroutineCuvette);
        _coroutineCuvette = StartCoroutine(MoveNewCuvette());
        _cuvetPanel.offsetMax=Vector2.zero;
        _cuvetPanel.offsetMin=Vector2.zero;
    }

    public void DownCuvettePanel()
    {
        CuvetPanel.DOLocalMove(_startPosCuvettePanel+Vector2.down*300f, 0.2f).SetEase(Ease.Linear).onComplete += EndDownCuvettePanel;
        ArrowDown.gameObject.SetActive(false);
    }

    private void EndDownCuvettePanel()
    {
        MoveBackCurrentCuvette();
    }

    public void Activate()
    {
        gameObject.SetActive(true);
        StartManager();
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }

    public void ActiveTrue()
    {
        gameObject.SetActive(true);
        _isMoveManager = false;
    }

    public void StartAnimArrow()
    {
        StopAnimArrow();
        _coroutineArrow = StartCoroutine(TimerAnimArrow());
    }

    IEnumerator TimerAnimArrow()
    {
        yield return new WaitForSeconds(2f);
        ArrowRight.transform.DOScale(Vector3.one * 1.2f, 0.2f).SetEase(Ease.OutCubic).SetLoops(4, LoopType.Yoyo).onComplete+=StartAnimArrow;
    }

    public void StopAnimArrow()
    {
        ArrowRight.transform.localScale=Vector3.one;
        if(_coroutineArrow==null) return;
        StopCoroutine(_coroutineArrow);
    }

    public void ShowBanner()
    {
        _parentPanel.offsetMin = new Vector2(0, (2400f/Screen.height)*170); //
        FonAds.gameObject.SetActive(true);
        FonAds.offsetMax = new Vector2(0, (2400f/Screen.height)*170);
    }

    public void HideBanner()
    {
        _parentPanel.offsetMin=Vector2.zero;
        FonAds.gameObject.SetActive(false);
    }

}
