﻿using UnityEngine;

public enum TouchState {Down, Drag, Up}

public class InputObject : MonoBehaviour
{
    [HideInInspector] public Transform Transform;
    [HideInInspector] public Vector2 FirstPosition;
    [HideInInspector] public Vector2 FirstGlobalPosition;
    [HideInInspector] public Vector2 SwipeDirection;
    [HideInInspector] public Vector2 PrevPos;
    [HideInInspector] public bool IsMovementAllowed = true;

    private bool _isFirstTouch = false;
    private Camera _camera;

    public virtual void Start()
    {
        Transform = transform;
        _camera=Camera.main;
    }

    public void OnMouseDown()
    {
#if !UNITY_EDITOR
         if (Input.touchCount != 1) return;
         PrevPos = _camera.ScreenToWorldPoint(Input.GetTouch(0).position);
#endif
#if UNITY_EDITOR
        PrevPos = _camera.ScreenToWorldPoint(Input.mousePosition);
#endif
       
        FirstPosition = Transform.localPosition;
        FirstGlobalPosition = Transform.position;
        _isFirstTouch = true;
        SwipeDirection = Vector2.zero;
        TouchDown();
    }

    public void OnMouseDrag()
    {
        if (_isFirstTouch)
        {
#if !UNITY_EDITOR
            Vector2 touchPos = _camera.ScreenToWorldPoint(Input.GetTouch(0).position);
            SwipeDirection = touchPos - PrevPos;
#endif
#if UNITY_EDITOR
            Vector2 touchPos = _camera.ScreenToWorldPoint(Input.mousePosition);
            SwipeDirection = touchPos - PrevPos;
#endif
            
            if (IsMovementAllowed)
                Transform.position = new Vector3(touchPos.x, touchPos.y, Transform.position.z);

            PrevPos = touchPos;
            TouchDrag();
        }
    }

    public void OnMouseUp()
    {
        if (_isFirstTouch)
        {
            _isFirstTouch = false;
            SwipeDirection = Vector2.zero;
            PrevPos = Vector2.zero;
            TouchUp();
        }
    }

    public virtual void TouchDown()
    {
        
    }
    
    public virtual void TouchDrag()
    {
        
    }

    public virtual void TouchUp()
    {
        
    }
}
