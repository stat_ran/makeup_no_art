﻿using UnityEngine;

public class Lips_Manager : Manager
{
    private Tool _prevTool;

    public override void Start()
    {
        base.Start();
        foreach (var tool in Tools)
        {
            tool.Init(this);
        }
    }
}
