﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PomadeGlamour_Tool : Tool, IDrawTool
{
    private Sprite _sprite;
    

    public override void Start()
    {
        base.Start();
        DeltaLeftPosition = 0.05f;
        DrawTool = this;
        IsEraser = true;
        foreach (var glossy in Data.LipsData.LipsGlamours)
        {
            GameObject obj = Instantiate(Data.LipsData.Prefab, DownMenu.ParentCuvet);
            obj.transform.localPosition=Vector3.zero;
            Item item = obj.GetComponent<Item>();
            Items.Add(item);
            item.Init(this, glossy.Icon);
            item.SetSprite(glossy.Sprite);
            item.SetStick(glossy.Stick);
            item.SetLockNumber(glossy.LockNumber);
            item.SetType(ManagersEnum.LipsGlamour);
        }
    }

    public override void Activate()
    {
        base.Activate();
        DownMenu.UpCuvettePanel(this);
        Manager.ActivateTutor();
    }

    public override void SetItem(Item item)
    {
        base.SetItem(item);
        IconTool.sprite = item.Stick;
    }

    public void Draw()
    {
        if (SelectedItem)
        {
            foreach (var targetImage in TargetImages)
            {
                Paint.DrawImage(targetImage, SelectedItem.Sprite);
            }
        }
    }
}
