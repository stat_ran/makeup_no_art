﻿using UnityEngine;

public class LipGloss_Tool : Tool, IDrawTool
{
    public SpriteRenderer TargetSpriteRenderer;
    public Sprite Gloss;

    public override void Start()
    {
        base.Start();
        IsEraser = true;
        DrawTool = this;
        TargetImages.Add(TargetSpriteRenderer);

        foreach (var targetImage in TargetImages)
        {
            Paint.ClearImage(targetImage);
        }
        
    }

    public override void Activate()
    {
        base.Activate();
        DownMenu.DownCuvettePanel();
        Manager.ActivateTutor();
    }

    public override void Init(Manager manager)
    {
        Manager = manager;
    }

    public void Draw()
    {
        foreach (var targetImage in TargetImages)
        {
            Paint.DrawImage(targetImage, Gloss);
        }
        
    }
    
}