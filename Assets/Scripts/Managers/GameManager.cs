﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AceCream.Scripts.Notification;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

//GameManager - точка входа
//DownMenu - Отвечает за нижнюю панель. Получает доступ ко всем менеджерам и сам их перелистывает.
//Manager - хранит ссылки на палитры, инструменты, таргеты(На чём рисуем). Передаёт в DownMenu Инструменты для размещения.
//Tool - Если выбран, то сам определяет где рисовать или что стирать. Передаёт в DownMenu Палитры и прочие Item для формирования верхней панели.

//1) Добавить Серёжки
//2) Добавить грим
//3) Добавить Стиралку
//4) НАучиться рисовать

//5) Добавить Тени
//6) Добавить брови
//7) Добавить ресницы
//8) Добавить румяны

//9) Добавить стразы



public enum ManagersEnum { Skin, Blush, Hair, Lens, Brows, Shadows, Lashes, Lips, Grim,  Crystal, Earring, ShadowsStandart, ShadowsGlamour, LipsMatt, LipsGlamour, LipsGlossy }

[System.Serializable]
public struct Managers
{
    public Skin_Manager Skin;
    public Blush_Manager Blush;
    public Hair_Manager Hair;
    public Lenses_Manager Lens;
    public Brows_Manager Brows;
    public Shadow_Manager Shadows;
    public Lashes_Manager Lashes;
    public Lips_Manager Lips;
    public Grim_Manager Grim;
    public Crystal_Manager Crystal;
    public Earring_Manager Earring;

    public Manager[] All
    {
        get
        {
            return new Manager[] { Skin, Blush, Hair, Lens, Brows, Shadows, Lashes, Lips, Grim, Crystal, Earring };
        }
    }

    public Manager Get(ManagersEnum managers)
    {
        return All[(int)managers];
    }
    
    public Manager Get(int i)
    {
        return All[i];
    }

    public ManagersEnum Index(Manager manager)
    {
        for (var i = 0; i < All.Length; i++)
        {
            if (manager == All[i])
                return (ManagersEnum)i;
        }

        return (ManagersEnum)(-1);
    }

    public int GetIndex(Manager manager)
    {
        for (var i = 0; i < All.Length; i++)
        {
            if (manager == All[i])
                return i;
        }

        return -1;
    }

    public void SetTypeManager(Manager manager)
    {
        if (manager is Skin_Manager skinManager) Skin = skinManager;
        if (manager is Blush_Manager blushManager) Blush = blushManager;
        if (manager is Hair_Manager hairManager) Hair = hairManager;
        if (manager is Lenses_Manager lensesManager) Lens = lensesManager;
        if (manager is Brows_Manager browsManager) Brows = browsManager;
        if (manager is Shadow_Manager shadowManager) Shadows = shadowManager;
        if (manager is Lashes_Manager lashesManager) Lashes = lashesManager;
        if (manager is Lips_Manager lipsManager) Lips = lipsManager;
        if (manager is Grim_Manager grimManager) Grim = grimManager;
        if (manager is Crystal_Manager crystalManager) Crystal = crystalManager;
        if (manager is Earring_Manager earringManager) Earring = earringManager;
    }
    
}

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameManager>();
            return _instance;
        }
    }

    public DataContainer Data;

    public Manager[] ManagersTool;

    [Header("Elements")] public Paint Paint;
    public Eraser Eraser;
    public GameObject Board;
    public Transform Girl;
    public Color ColorOutlineTools;
    public Color ColorOutlineCuvete;

    [Header("UI")] public UIBar UIBar;
    public StartMenu StartMenu;

    [Header("Other")] public Winking Winking;
    public Material BlinkMat;

    [Space] public Transform ManagerParent;
    [HideInInspector] public Managers Managers;

    public ParticleSystem Particle;
    public ParticleSystem Particle2;
    //public GameObject ColliderBlush;
    public Sprite LockSprite;

    [HideInInspector] public Manager CurrentManager;
    [HideInInspector] public Tool ActiveTool;
    [HideInInspector] public ManagersEnum managers = ManagersEnum.Skin;
    [HideInInspector] public int NumberGame;
    [HideInInspector] public int CurrentTool;
    [HideInInspector] public DownMenu DownMenu;
    public ParticleClass ParticleStars;
    [HideInInspector] public Color ColorTools;
    public AudioClip StarAudioClip;
    public AudioSource AudioSource;
    public GiftPanel GiftPanel;

    [Space] public Animator PhotoAnim;

    public SpriteRenderer[] TutorSpriteRenderers;
    public GameObject[] Items;
    [HideInInspector] public SpriteRenderer CurrentPaintSprite;

    private int _frameCount = 0;
    private float _timeCount = 0;
    public bool IsGamePlay;
    private List<List<Item>> _items;
    private Vector2 _startScaleGirl;
    private Vector2 _startPositionGirl;

    public GameObject TEstSaveImage;

    public void Awake() //override
    {
        //base.Awake();
        Manager[] managers = ManagerParent.GetComponentsInChildren<Manager>(true);
        foreach (var manager in managers)
        {
            Managers.SetTypeManager(manager);
        }

        _startPositionGirl = Girl.position;
        _startScaleGirl = Girl.localScale;
    }


    public void Start() //override
    {
        //TotalClear(); //TODO Очистка девочек на старте(Надо ли???)
        //PlayerPrefs.DeleteAll();
        DownMenu.Init();
        StartMenu.Activate(true);
        StopEffect(true);
        //ColliderBlush.SetActive(false);
        //InitItems();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            Debug.Log(SaveImageToMemory.SaveImage(Camera.main));
        }

        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            Image[] images = TEstSaveImage.GetComponentsInChildren<Image>();
            var sprites = SaveImageToMemory.GetAllSprites();
            for (int i = 0; i < sprites.Length; i++)
            {
                images[i].sprite = sprites[i];
            }
        }
    }

    public void Init()
    {
        RandomGirl();
    }

    public void DeInit()
    {
        foreach (Manager manager in Managers.All)
        {
            manager.DeInit();
        }
    }

    public void SetManager(Manager manager)
    {
        CurrentManager = manager;
    }

    public void ScreenAction(TouchState state, InputObject obj)
    {
        // if (ActiveTool)
        //     ActiveTool.OnScreen(state, obj);
    }

    public void TotalClear()
    {
        foreach (Manager manager in Managers.All)
        {
            manager.DeInit();
            manager.Clear();
        }

        CurrentManager = null;
    }

    public void RandomGirl()
    {
        if (CurrentManager) CurrentManager.DeInit();
        Managers.Get(ManagersEnum.Skin).Randomizer();
        Managers.Get(ManagersEnum.Hair).Randomizer();
        Managers.Get(ManagersEnum.Lens).Randomizer();
        Managers.Get(ManagersEnum.Earring).Randomizer();
        Managers.Get(ManagersEnum.Brows).Randomizer();
        Managers.Get(ManagersEnum.Crystal).Clear();
        Managers.Get(ManagersEnum.Brows).Clear();
        Managers.Get(ManagersEnum.Blush).Clear();
        Managers.Get(ManagersEnum.Grim).Clear();
        Managers.Get(ManagersEnum.Lips).Clear();
        Managers.Get(ManagersEnum.Lashes).Clear();
        Managers.Get(ManagersEnum.Shadows).Clear();
    }

    public virtual void CorrectPosition(Vector2 position)
    {
        Particle.transform.position = position;
    }

    public virtual void PlayEffect()
    {
        if (Particle.isEmitting) return;
        Particle.Play(true);
    }

    public virtual void StopEffect(bool isClear)
    {
        if (!Particle.isEmitting) return;
        Particle.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
    }

    public void SetColor(Color color)
    {
        var m = Particle2.main;
        m.startColor = color;
    }

    private void InitItems()
    {
        if (MySDK.Instance.NoAds) return;
        _items = new List<List<Item>>();
        foreach (var t in Items)
        {
            List<Item> list = t.GetComponentsInChildren<Item>(true).ToList();
            foreach (var item in list)
            {
                if (item.IsLock)
                {
                    _items.Add(list);
                    break;
                }
            }
        }
    }

    public Item GetRandomLockItem()
    {
        if (MySDK.Instance.NoAds) return null;
        int ran = Random.Range(0, _items.Count);
        foreach (var item in _items[ran])
        {
            if (item.IsLock)
            {
                return item;
            }
        }

        foreach (var t in _items)
        {
            List<Item> list = t;
            foreach (var item in list)
            {
                if (!item.IsLock)
                {
                    continue;
                }

                _items.Remove(list);
            }
        }

        return null;
    }

    public Item GetGift()
    {
        if (CurrentTool != 7) return null;
        NumberGame++;
        if (NumberGame == 1)
        {
            Item item = GetRandomLockItem();
            Debug.Log("1 " + item.name);
            PlayerPrefs.SetInt(item.name, 1);
            PlayerPrefs.Save();
            NotificationCenter.postNotification(Notifications.CheckBuy, null);
            GiftPanel.Open(item);
            return item;
        }

        if (NumberGame % 5 == 0)
        {

            Item item = GetRandomLockItem();
            Debug.Log("5 " + item.name);
            PlayerPrefs.SetInt(item.name, 1);
            PlayerPrefs.Save();
            NotificationCenter.postNotification(Notifications.CheckBuy, null);
            GiftPanel.Open(item);
            return item;
        }

        return null;

    }

    public void StartGirl()
    {
        Girl.position = _startPositionGirl;
        Girl.localScale = _startScaleGirl;
    }

    public void PlaySound(AudioClip clip)
    {
        AudioSource.PlayOneShot(clip);
    }

}


