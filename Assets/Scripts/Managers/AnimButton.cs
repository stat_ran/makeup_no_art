using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class AnimButton : BaseBahavior
{

    [HideInInspector] public Action ClickButton;
    private Ease Ease=Ease.OutCubic;
    private Vector2 _startScale;
    
    public override void Start()
    {
        base.Start();
        _startScale = transform.localScale;
        GetComponent<Button>().onClick.AddListener(Click);
    }

    private void Click()
    {
        transform.DOKill();
        transform.localScale=_startScale;
        transform.DOScale(_startScale * 1.1f, 0.1f).SetEase(Ease).SetLoops(2, LoopType.Yoyo);
    }

}
