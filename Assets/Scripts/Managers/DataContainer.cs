using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;

//{ Skin, Blush, Hair, Lens, Brows, Shadows, Lashes, Lips, Grim,  Crystal, Earring }
[CreateAssetMenu(fileName = "DataContainer", menuName = "Data", order = 51)]
public class DataContainer : ScriptableObject
{

    public SkinData SkinData;       //Кожа
    public BlushData BlushData;     //Румяны
    public HairData HairData;       //Волосы
    public LensData LensData;       //Линзы
    public BrowsData BrowsData;     //Брови
    public ShadowsData ShadowsData; //Тени
    public LashesData LashesData;   //Ресницы
    public LipsData LipsData;       //Губы
    public GrimData GrimData;       //Грим
    public CrystalData CrystalData; //Стразы
    public EarringData EarringData; //Серёжки
    public ColorData ColorData;     //Набор Цветов
}

[System.Serializable]
public class SkinData
{
    [HideInInspector] public ManagersEnum ManagersEnum = ManagersEnum.Skin;
    public SkinEyelid[] SkinEyelids;
    
    [System.Serializable]
    public class SkinEyelid
    {
        public Sprite Skin;
        public Sprite Eyelid;
    }
}

[System.Serializable]
public class BlushData
{
    [HideInInspector] public ManagersEnum ManagersEnum = ManagersEnum.Blush;
    public GameObject Prefab;
    public Blush[] Blushes;
    
    [System.Serializable]
    public class Blush
    {
        public Sprite Icon;
        public Sprite Sprite;
        public int LockNumber;
    }
}

[System.Serializable]
public class HairData
{
    [HideInInspector] public ManagersEnum ManagersEnum = ManagersEnum.Hair;
    public GameObject Prefab;
    public Hair[] Hairs;
    
    [System.Serializable]
    public class Hair
    {
        public Sprite Icon;
        public Sprite Front;
        public Sprite Back;
        public int LockNumber;
    }
}

[System.Serializable]
public class LensData
{
    [HideInInspector] public ManagersEnum ManagersEnum = ManagersEnum.Lens;
    public GameObject Prefab;
    public Lens[] Lenses;
    
    [System.Serializable]
    public class Lens
    {
        public Sprite Sprite;
        public int LockNumber;
    }
}

[System.Serializable]
public class BrowsData
{
    [HideInInspector] public ManagersEnum ManagersEnum = ManagersEnum.Brows;
    public GameObject PrefabTool;
    public GameObject PrefabCuvette;
    public Brows[] Browses;
    
    [System.Serializable]
    public class Brows
    {
        public Sprite Icon;
        public Sprite Sprite;
        public int LockNumber;
    }
}

[System.Serializable]
public class ShadowsData
{
    [HideInInspector] public ManagersEnum ManagersEnum = ManagersEnum.Shadows;
    public GameObject Prefab;
    public ShadowStandard[] ShadowStandards;
    public ShadowGlamour[] ShadowGlamours;
    
    [System.Serializable]
    public class ShadowStandard
    {
        public Sprite Icon;
        public Sprite Sprite;
        public int LockNumber;
    }
    
    [System.Serializable]
    public class ShadowGlamour
    {
        public Sprite Icon;
        public Sprite Sprite;
        public int LockNumber;
    }
}

[System.Serializable]
public class LashesData
{
    [HideInInspector] public ManagersEnum ManagersEnum = ManagersEnum.Lashes;
    public GameObject PrefabTool;
    public GameObject PrefabCuvette;
    public Lashes[] Lasheses;
    
    [System.Serializable]
    public class Lashes
    {
        public Sprite Icon;
        public Sprite Sprite;
        public int LockNumber;
    }
}

[System.Serializable]
public class LipsData
{
    [HideInInspector] public ManagersEnum ManagersEnum = ManagersEnum.Lips;
    public GameObject Prefab;
    public LipsGlamour[] LipsGlamours;
    public LipsGlossy[] LipsGlossies;
    public LipsMatt[] LipsMatts;
    
    [System.Serializable]
    public class LipsGlossy
    {
        public Sprite Icon;
        public Sprite Stick;
        public Sprite Sprite;
        public int LockNumber;
    }
    [System.Serializable]
    public class LipsGlamour
    {
        public Sprite Icon;
        public Sprite Stick;
        public Sprite Sprite;
        public int LockNumber;
    }
    
    [System.Serializable]
    public class LipsMatt
    {
        public Sprite Icon;
        public Sprite Stick;
        public Sprite Sprite;
        public int LockNumber;
    }
}

[System.Serializable]
public class GrimData
{
    [HideInInspector] public ManagersEnum ManagersEnum = ManagersEnum.Grim;
    public GameObject Prefab;
    public Grim[] Grims;
    
    [System.Serializable]
    public class Grim
    {
        public Sprite Icon;
        public Sprite Sprite;
        public int LockNumber;
    }
}

[System.Serializable]
public class CrystalData
{
    [HideInInspector] public ManagersEnum ManagersEnum = ManagersEnum.Crystal;
    public Crystal[] Crystals;
    
    [System.Serializable]
    public class Crystal
    {
        public GameObject Prefab;
        public GameObject ScrollPrefab;
        public int LockNumber;
    }
}

[System.Serializable]
public class EarringData
{
    [HideInInspector] public ManagersEnum ManagersEnum = ManagersEnum.Earring;
    public GameObject Prefab;
    public Earring[]  Earrings;
    
    [System.Serializable]
    public class Earring
    {
        public Sprite Icon;
        public Sprite Sprite;
        public Sprite Outline;
        public int LockNumber;
    }
}

[System.Serializable]
public class ColorData
{
    public ColorCuvette[] ColorCuvettes;
    
    [System.Serializable]
    public class ColorCuvette
    {
        public Sprite Icon;
        public Color Color;
    }
}
