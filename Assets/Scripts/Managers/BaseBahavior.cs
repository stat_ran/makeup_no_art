using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBahavior : MonoBehaviour
{
    [HideInInspector] public Camera MainCamera;
    [HideInInspector] public GameManager GM;
    [HideInInspector] public DataContainer Data;
    [HideInInspector] public DownMenu DownMenu;
    [HideInInspector] public Paint Paint;
    [HideInInspector] public Photo_Album Album;

    private Transform _cachedTransform;
    private bool _transformIsCached;
    
    public new Transform transform
    {
        get
        {
            if (_transformIsCached) return _cachedTransform;
            _cachedTransform = base.transform;
            _transformIsCached = _cachedTransform != null;
            return _cachedTransform;
        }
    }
    
    public virtual void Awake()
    {
        MainCamera=Camera.main;
        GM=GameManager.Instance;
        Data = GM.Data;
        Paint = GM.Paint;
        DownMenu = FindObjectOfType<DownMenu>(true);
        Album = FindObjectOfType<Photo_Album>(true);
        GM.DownMenu = DownMenu;
    }

    public virtual void Start()
    {
        
    }


    public Vector2 MousePosition
    {
        get
        {
#if UNITY_EDITOR
            return MainCamera.ScreenToWorldPoint(Input.mousePosition);
#else
            return  MainCamera.ScreenToWorldPoint(Input.GetTouch(0).position);
#endif
        }
    }
}
