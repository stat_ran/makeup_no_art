using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoSticker_Tool : PhotoTool
{
    public Transform Target;
    public Sprite[] Stickers;
    public GameObject Prefab;
    public GameObject PrefabSticker;

    [HideInInspector] public List<PhotoSticker> PhotoStickers;
    
    public override void Start()
    {
        base.Start();
        Type = PhotoItem.PhotoEnum.Sticker;
        foreach (var sprite in Stickers)
        {
            GameObject obj = Instantiate(Prefab, Manager.ParentCuvet);
            obj.transform.localPosition=Vector3.zero;
            PhotoItem item = obj.GetComponent<PhotoItem>();
            Items.Add(item);
            item.InitSticker(this, sprite, PrefabSticker);
        }
    }

    public override void SetItem(PhotoItem item)
    {
        base.SetItem(item);
        if(!item) return;
        PhotoSticker sticker = Instantiate(item.PrefabSticker).GetComponent<PhotoSticker>();
        PhotoStickers.Add(sticker);
        sticker.transform.position = Target.position;
        sticker.Init(item.Icon.sprite, this);
    }

    public override void Clear()
    {
        base.Clear();
        if (PhotoStickers.Count > 0)
        {
            foreach (var sticker in PhotoStickers)
            {
                Destroy(sticker.gameObject);
            }
            PhotoStickers.Clear();
        }
    }
}
