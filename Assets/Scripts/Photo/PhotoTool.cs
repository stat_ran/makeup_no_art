using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotoTool : BaseBahavior
{
    public PhotoItem.PhotoEnum Type;
    private Image _outline;
    [HideInInspector] public Photo_Manager Manager;
    [HideInInspector] public List<PhotoItem> Items;
    [HideInInspector] public PhotoItem SelectItem;

    public override void Start()
    {
        base.Start();
        _outline = GetComponent<Image>();
        GetComponent<Button>().onClick.AddListener(Activate);
        Deactivate();
    }

    public void Init(Photo_Manager manager)
    {
        Manager = manager;
    }

    public virtual void Activate()
    {
        if(Manager.SelectTool==this) return;
        if(Manager.SelectTool) Manager.SelectTool.Deactivate();
        Manager.SelectTool = this;
        _outline.enabled = true;
        Manager.SetCuvete(this);
        transform.localScale = Vector3.one * 1.2f;
    }

    public virtual void Deactivate()
    {
        if(_outline) _outline.enabled = false;
        transform.localScale = Vector3.one;
    }

    public virtual void SetItem(PhotoItem item)
    {
        SelectItem = item;
    }

    public virtual void Clear()
    {
        Deactivate();
    }

}
