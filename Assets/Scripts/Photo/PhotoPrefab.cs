using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotoPrefab : BaseBahavior
{

    public Image Photo;
    private RectTransform _rectTransform;

    public void Init(Sprite sprite, int number)
    {
        Photo.sprite = sprite;
        _rectTransform = GetComponent<RectTransform>();
        StartCoroutine(TimerForFrame(number));
        GetComponent<Button>().onClick.AddListener(Activate);
    }

    IEnumerator TimerForFrame(int number)
    {
        yield return new WaitForEndOfFrame();
        number++;
        float delta = -(_rectTransform.offsetMin.y - _rectTransform.offsetMax.y) * (200f / 213f);
        _rectTransform.offsetMin = new Vector2( delta*(number-1), _rectTransform.offsetMin.y);
        _rectTransform.offsetMax = new Vector2( delta*number, _rectTransform.offsetMax.y);
    }

    private void Activate()
    {
        if(Album.CurrentPhoto) Album.CurrentPhoto.Deactivate();
        Album.CurrentPhoto = this;
        Album.ImageFrame.sprite = Photo.sprite;
        transform.localScale = Vector3.one * 1.2f;
    }

    private void Deactivate()
    {
        transform.localScale = Vector3.one;
    }

}
