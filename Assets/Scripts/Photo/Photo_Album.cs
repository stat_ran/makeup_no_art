using System;
using System.Collections;
using System.Collections.Generic;
using AceCream.Scripts.Notification;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Photo_Album : BaseBahavior
{

    public static Photo_Album Instance;
    
    public GameObject PhotoPanel;
    public GameObject Prefab;
    public Transform  Container;
    public GameObject BackButton;
    public Image ImageFrame;

    public Button Back_Button;
    public Button Home_Button;
    public Button Shop_Button;
    
    [HideInInspector] public PhotoPrefab CurrentPhoto;

    private Vector3 _startPosition;
    private Sprite _firstSprite;
    private List<PhotoPrefab> _photoPrefabs=new List<PhotoPrefab>();

    public override void Awake()
    {
        base.Awake();
        if (Instance == null) Instance = this;
        Back_Button.onClick.AddListener(Back_Click);
        Home_Button.onClick.AddListener(Home_Click);
        Shop_Button.onClick.AddListener(Shop_Click);
        PhotoPanel.SetActive(true);
        _startPosition = PhotoPanel.transform.position;
        PhotoPanel.transform.position = _startPosition + Vector3.right * 10f;
    }

    public override void Start()
    {
        base.Start();
        InitAlbum();
        //PhotoPanel.SetActive(false);
        Deactivate();
    }
    
    public void Activate()
    {
        PhotoPanel.SetActive(true);
        PhotoPanel.transform.position = _startPosition + Vector3.right * 10f;
        PhotoPanel.transform.DOMove(_startPosition, 0.4f).SetEase(Ease.Linear);
        //InitAlbum();
        ImageFrame.sprite = _firstSprite;
        Back_Button.gameObject.SetActive(false);
        StartCoroutine(MoveNewAlbum());
    }

    public void Activate(Sprite sprite)
    {
        PhotoPanel.SetActive(true);
        PhotoPanel.transform.position = _startPosition + Vector3.right * 10f;
        PhotoPanel.transform.DOMove(_startPosition, 0.4f).SetEase(Ease.Linear);
        //InitAlbum();
        ImageFrame.sprite = sprite;
        Back_Button.gameObject.SetActive(true);
        StartCoroutine(MoveNewAlbum());
    }

    private void Deactivate()
    {
        PhotoPanel.transform.DOMove(_startPosition + Vector3.right * 10f, 0.4f).SetEase(Ease.Linear).onComplete +=
            SetActiveFalse;
    }

    private void SetActiveFalse()
    {
        //PhotoPanel.SetActive(false);
        StartCoroutine(WaitForTwoFrame());
    }

    IEnumerator WaitForTwoFrame()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        PhotoPanel.SetActive(false);
    }

    IEnumerator MoveNewAlbum()
    {
        float speed = 2f;

        ScrollRect scrollRect = Container.GetComponentInParent<ScrollRect>();
        scrollRect.horizontalNormalizedPosition = 1f;
        while (true)
        {
            scrollRect.horizontalNormalizedPosition -= Time.deltaTime * speed;
            yield return null;
            if (scrollRect.horizontalNormalizedPosition <= 0)
            {
                yield break;
            }
        }
        yield break;
    }

    private void InitAlbum() //Делаем на старте!
    {
        Sprite[] sprites = SaveImageToMemory.GetAllSprites();

        for (int i = 0; i < sprites.Length; i++)
        {
            GameObject obj = Instantiate(Prefab, Container);
            PhotoPrefab prefab=obj.GetComponent<PhotoPrefab>();
            prefab.Init(sprites[i], i);
            _photoPrefabs.Add(prefab);
        }

        if (sprites.Length > 0) _firstSprite = sprites[0];
    }

    public void AddPhoto(Sprite sprite)
    {
        GameObject obj = Instantiate(Prefab, Container);
        PhotoPrefab prefab=obj.GetComponent<PhotoPrefab>();
        prefab.Init(sprite, _photoPrefabs.Count);
        _photoPrefabs.Add(prefab);
    }

    private void Back_Click()
    {
        Deactivate();
        Photo_Manager.Instance.Activate();
    }

    private void Home_Click()
    {
        Deactivate();
        StartMenu.Instance.Activate(true);
    }

    private void Shop_Click()
    {
        NotificationCenter.postNotification(Notifications.OpenStore, null);
    }

}
