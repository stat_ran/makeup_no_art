using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotoFon_Tool : PhotoTool
{
   public SpriteRenderer TargetSpriteRenderer;
   public GameObject Prefab;
   public Sprite[] SpritesFon;
   
   private Image Outline;

   public override void Start()
   {
      base.Start();
      Type = PhotoItem.PhotoEnum.Fon;
      foreach (var sprite in SpritesFon)
      {
         GameObject obj = Instantiate(Prefab, Manager.ParentCuvet);
         obj.transform.localPosition=Vector3.zero;
         PhotoItem item = obj.GetComponent<PhotoItem>();
         Items.Add(item);
         item.InitFon(this, sprite);
      }
   }

   public override void SetItem(PhotoItem item)
   {
      base.SetItem(item);
      if(item) TargetSpriteRenderer.sprite = item.Icon.sprite;
   }

   public override void Clear()
   {

   }
}
