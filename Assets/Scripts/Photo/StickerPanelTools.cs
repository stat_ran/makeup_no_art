using UnityEngine;
using UnityEngine.UI;

public class StickerPanelTools : BaseBahavior
{

    public Button RotateLeft;
    public Button RotateRight;
    public Button ScaleUp;
    public Button ScaleDown;
    public Button ChangeLayer;
    public Button Delete;
    public Button Deactivate;

    private PhotoSticker _sticker;
    private Vector2 _startPosition;

    public override void Awake()
    {
        base.Awake();
        RotateLeft.onClick.AddListener(RotateLeft_Click);
        RotateRight.onClick.AddListener(RotateRight_Click);
        ScaleDown.onClick.AddListener(ScaleDown_Click);
        ScaleUp.onClick.AddListener(ScaleUp_Click);
        ChangeLayer.onClick.AddListener(ChangeLayer_Click);
        Delete.onClick.AddListener(Delete_Click);
        Deactivate.onClick.AddListener(Deactivate_Click);
        gameObject.SetActive(false);
    }
    
    public void Activate(PhotoSticker sticker)
    {
        _sticker = sticker;
        gameObject.SetActive(true);
    }

    private void RotateLeft_Click()
    {
        _sticker.transform.eulerAngles += Vector3.forward * 15f;
    }

    private void RotateRight_Click()
    {
        _sticker.transform.eulerAngles += Vector3.back * 15f;
    }

    private void ScaleUp_Click()
    {
        if(_sticker.transform.localScale.x>1.29f) return;
        _sticker.transform.localScale+=Vector3.one*0.1f;
    }

    private void ScaleDown_Click()
    {
        if(_sticker.transform.localScale.x<0.79f) return;
        _sticker.transform.localScale-=Vector3.one*0.1f;
    }

    private void Delete_Click()
    {
        _sticker.Delete();
        Deactivate_Click();
    }

    private void ChangeLayer_Click()
    {
        _sticker.SpriteRenderer.sortingLayerName = _sticker.SpriteRenderer.sortingLayerName == "Default" ? "Girl_HairFront" : "Default";
    }

    public void Deactivate_Click()
    {
        gameObject.SetActive(false);
    }

}
