using UnityEngine;
using UnityEngine.EventSystems;

public class PhotoSticker : BaseBahavior
{

   [HideInInspector] public SpriteRenderer SpriteRenderer;
   private bool _isClick;
   private Vector3 _vector3=Vector3.zero;
   private PhotoSticker_Tool _tool;

   public void Init(Sprite sprite, PhotoSticker_Tool tool)
   {
      _tool = tool;
      SpriteRenderer = GetComponent<SpriteRenderer>();
      SpriteRenderer.sprite = sprite;
      _vector3.z = -5f;
      gameObject.AddComponent<BoxCollider2D>();
      Photo_Manager.Instance.StickerPanelTools.Activate(this);
   }

   private void OnMouseDown()
   {
      PointerEventData eventData = new PointerEventData(EventSystem.current);
      eventData.position = Input.mousePosition;
      if (!EventSystem.current.IsPointerOverGameObject())
      {
         _isClick = true;
         _vector3.x = MousePosition.x;
         _vector3.y = MousePosition.y;
         transform.position = _vector3;
      }
      Photo_Manager.Instance.StickerPanelTools.Activate(this);
   }

   private void OnMouseDrag()
   {
      PointerEventData eventData = new PointerEventData(EventSystem.current);
      eventData.position = Input.mousePosition;
      if (!EventSystem.current.IsPointerOverGameObject())
      {
         _vector3.x = MousePosition.x;
         _vector3.y = MousePosition.y;
         transform.position = _vector3;
      }
   }

   private void OnMouseUp()
   {
      _isClick = false;
   }

   public void Delete()
   {
      if (_tool.PhotoStickers.Contains(this))
         _tool.PhotoStickers.Remove(this);
      Destroy(gameObject);
   }
}
