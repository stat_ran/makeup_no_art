using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoEffect_Tool : PhotoTool
{
   public Transform TargetPoint;
   public GameObject Prefab;

   public ItemsEffect[] ItemsEffects;
   
   private GameObject _effect;

   public override void Start()
   {
      base.Start();
      Type = PhotoItem.PhotoEnum.Effect;
      foreach (var itemsEffect in ItemsEffects)
      {
         GameObject obj = Instantiate(Prefab, DownMenu.ParentCuvet);
         obj.transform.localPosition = Vector3.zero;
         PhotoItem item = obj.GetComponent<PhotoItem>();
         Items.Add(item);
         item.InitEffect(this, itemsEffect.Icon, itemsEffect.PrefabEffect);
      }
   }

   public override void SetItem(PhotoItem item)
   {
      base.SetItem(item);
      if(_effect) Destroy(_effect);
      if(!item) return;
      _effect = Instantiate(item.Effect);
      Manager.CurrentEffect = _effect;
      _effect.transform.position = TargetPoint.position;
      _effect.SetActive(true);
   }

   public override void Clear()
   {
      base.Clear();
      if(_effect) Destroy(_effect);
   }
}

[System.Serializable]
public class ItemsEffect
{
   public GameObject PrefabEffect;
   public Sprite Icon;
}
