using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Random = UnityEngine.Random;

// Texture2D SaveImage(Camera camera, string name="") - Сохраняет изображение с камеры, если не вводиться имя, то имя рандомное
// Sprite GetSprite(string name)                   - Получить спрайт по имени
// Sprite[] GetAllSprites()                        - Получить все сохранённые спрайты!
// Texture2D GetTexture(string name)               - получить текстуру сохрранённого изображения.
// string[] GetAllName()                           - Получить массив имён всех сохранённых скринов
// bool HasName(string name)                       - Существует ли скриншот с таким именем
// DeleteImage(string name)                        - удалить изображение
// Texture2D CreateScreenShoot(Camera camera)      - Создаёт текстуру но не сохраняет в память.

public static class SaveImageToMemory
{

    private static string _namePP = "gallery";
    private static string _path;
    private static NameImages _nameImages=new NameImages();

    public static Texture2D SaveImage(Camera camera, string name="")
    {
        CheckSave();
        
        if (name == "") name = Random.Range(float.MinValue, float.MaxValue).ToString();

        Texture2D texture2D = CreateScreenShoot(camera);
        
        File.WriteAllBytes($"{Application.persistentDataPath}/{name}.png", texture2D.EncodeToPNG());
        camera.targetTexture = null;
        
        _nameImages.NameSizes.Add(new SizeNameImage(name, camera.pixelWidth, camera.pixelHeight));
        PlayerPrefs.SetString(_namePP, JsonUtility.ToJson(_nameImages));
        PlayerPrefs.Save();

        return texture2D;
    }
    
    

    public static Sprite[] GetAllSprites()
    {
        CheckSave();
        List<Sprite> sprites=new List<Sprite>();
        foreach (var nameSiz in _nameImages.NameSizes)
        {
            sprites.Add(GetSprite(nameSiz.Name));
        }

        return sprites.ToArray();
    }

    private static Texture2D LoadTexture(SizeNameImage sizeNameImage)
    {
        var filePath = $"{Application.persistentDataPath}/{sizeNameImage.Name}.png";
        if (File.Exists(filePath)){
            var fileData = File.ReadAllBytes(filePath);
            var tex2D = new Texture2D(sizeNameImage.Width, sizeNameImage.Height);
            if (tex2D.LoadImage(fileData))
                return tex2D;
        }  
        return null;
    }

    public static Sprite GetSprite(string name)
    {
        SizeNameImage sizeNameImage=null;
        foreach (var nameSiz in _nameImages.NameSizes)
        {
            if (nameSiz.Name == name)
            {
                sizeNameImage = nameSiz;
                break;
            }
        }

        if (sizeNameImage==null) return null;
        var texture = LoadTexture(sizeNameImage);
        if (texture == null) return null;
        var pic = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height),
            new Vector2(0.5f, 0.5f), 100);
        return pic;
    }

    public static Texture2D GetTexture(string name)
    {
        SizeNameImage sizeNameImage=null;
        foreach (var nameSiz in _nameImages.NameSizes)
        {
            if (nameSiz.Name == name)
            {
                sizeNameImage = nameSiz;
                break;
            }
        }
        if (sizeNameImage==null) return null;
        
        return LoadTexture(sizeNameImage);
    }

    public static string[] GetAllName()
    {
        CheckSave();
        List<string> names=new List<string>();
        foreach (var nameSiz in _nameImages.NameSizes)
        {
            names.Add(nameSiz.Name);
        }
        return names.ToArray();
    }

    public static bool HasName(string name)
    {
        CheckSave();
        foreach (var nameSiz in _nameImages.NameSizes)
        {
            if (nameSiz.Name == name) return true;
        }
        return false;
    }

    public static void DeleteImage(string name)
    {
        CheckSave();
        foreach (var nameSiz in _nameImages.NameSizes)
        {
            if (nameSiz.Name == name)
            {
                _nameImages.NameSizes.Remove(nameSiz);
                break;
            }
        }
        
        PlayerPrefs.SetString(_namePP, JsonUtility.ToJson(_nameImages));
        PlayerPrefs.Save();
    }

    public static Texture2D CreateScreenShoot(Camera camera)
    {
        //var renderTexture = new RenderTexture(camera.pixelWidth, camera.pixelHeight, (int) camera.depth);
        //camera.targetTexture = renderTexture;
        float y = camera.rect.y;
        camera.rect=new Rect(0,0,camera.rect.width, camera.rect.height);
        camera.Render();
        RenderTexture.active = camera.targetTexture;
        Texture2D texture2D =
            new Texture2D(camera.pixelWidth, camera.pixelHeight, TextureFormat.RGB24, false);
        texture2D.ReadPixels(new Rect(0, 0, camera.pixelWidth, camera.pixelHeight), 0, 0);
        texture2D.Apply();
        return texture2D;
    }
    
    public static Sprite CreateScreenShoot_Sprite(Camera camera)
    {
        Texture2D texture2D = CreateScreenShoot(camera);
        var pic = Sprite.Create(texture2D, new Rect(0.0f, 0.0f, camera.pixelWidth, camera.pixelHeight),
            new Vector2(0.5f, 0.5f), 100);
        return pic;
    }

    public static void SaveTexture(Texture2D texture2D)
    {
        CheckSave();
        var name = Random.Range(float.MinValue, float.MaxValue).ToString();
        File.WriteAllBytes($"{Application.persistentDataPath}/{name}.png", texture2D.EncodeToPNG());

        _nameImages.NameSizes.Add(new SizeNameImage(name, texture2D.width, texture2D.height));
        PlayerPrefs.SetString(_namePP, JsonUtility.ToJson(_nameImages));
        PlayerPrefs.Save();
    }

    private static void CheckSave()
    {
        if (_nameImages.NameSizes.Count == 0 && PlayerPrefs.HasKey(_namePP))
        {
            _nameImages = JsonUtility.FromJson<NameImages>(PlayerPrefs.GetString(_namePP));
        }
    }
    
}

[System.Serializable]
public class NameImages
{
    public List<SizeNameImage> NameSizes=new List<SizeNameImage>();
        
}
    
[System.Serializable]
public class SizeNameImage
{
    public string Name;
    public int Width;
    public int Height;

    public SizeNameImage(string name, int width, int height)
    {
        Name = name;
        Width = width;
        Height = height;
    }
}