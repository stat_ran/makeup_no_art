using System.Collections;
using System.Collections.Generic;
using AceCream.Scripts.Notification;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class Photo_Manager : BaseBahavior
{
    public static Photo_Manager Instance;
    public Camera Camera;
    public GameObject PhotoFon;
    public GameObject BackGameButton;
    public PhotoTool[] PhotoTools;
    public Transform ContainerCuvet;
    public Transform ConteinerTools;
    public Transform ParentCuvet;
    public GameObject Photo_CuvetPanel;
    public GameObject Photo_ToolsPanel;
    public Button PhotoButton_Level1;

    //Левел 2
    [Header("Level2")] public GameObject Photo2;
    public Image Photo;
    public Button ButtonPhoto;
    public GameObject Effect;

    //Левел 3
    [Header("Level3")] public GameObject CencelButton;
    public GameObject SaveButton;
    public GameObject AlbomButton;

    public GameObject StarEffect;
    public AudioClip FlashSoundEffect;
    

    [HideInInspector] public PhotoTool SelectTool;
    [HideInInspector] public GameObject CurrentEffect;
    [HideInInspector] public StickerPanelTools StickerPanelTools;

    private ScrollRect _scrollCuvettePanel;
    private bool _isMoveManager;
    private Vector2 _startPositionFrame;
    private Coroutine _coroutine;

    public override void Awake()
    {
        base.Awake();
        if (Instance == null) Instance = this;
        foreach (var photoTool in PhotoTools)
        {
            photoTool.Init(this);
        }

        Photo_CuvetPanel.SetActive(true);
        Photo_ToolsPanel.SetActive(true);
        _scrollCuvettePanel = ContainerCuvet.GetComponentInParent<ScrollRect>();
        StickerPanelTools = FindObjectOfType<StickerPanelTools>(true);
        _startPositionFrame = Photo.transform.parent.position;
       
    }

    public override void Start()
    {
        base.Start();
        Deactivate();
        //CorrectSizeCamera();
    }

    public void Activate()
    {
        FlashEffectDeactivate();
        DownMenu.Deactivate();
        Photo_CuvetPanel.SetActive(true);
        Photo_ToolsPanel.SetActive(true);
        ButtonPhoto.gameObject.SetActive(true);
        Photo.transform.parent.position = _startPositionFrame;
        Photo.transform.parent.localScale = Vector3.one;
        Photo.transform.parent.eulerAngles = Vector3.zero;
        PhotoFon.SetActive(true);
        Photo2.SetActive(false);
        BackGameButton.SetActive(true);
        GM.IsGamePlay = false;
        StartCoroutine(WaitFrame());
        PhotoButton_Level1.transform.DOScale(PhotoButton_Level1.transform.localScale * 1.1f, 0.1f)
            .SetEase(Ease.OutCubic)
            .SetLoops(2, LoopType.Yoyo);
        GM.Girl.localScale = Vector3.one * 0.65f;
        GM.Girl.position = Vector3.down * 0.5f;
    }

    IEnumerator WaitFrame()
    {
        yield return new WaitForEndOfFrame();
        PhotoTools[0].Activate();
    }

    public void Deactivate()
    {
        FlashEffectDeactivate();
        Photo_CuvetPanel.SetActive(false);
        Photo_ToolsPanel.SetActive(false);
        ButtonPhoto.gameObject.SetActive(false);
        CencelButton.SetActive(false);
        SaveButton.SetActive(false);
        AlbomButton.SetActive(false);
        Effect.SetActive(false);
        Photo2.SetActive(false);
        PhotoFon.SetActive(false);
        BackGameButton.SetActive(false);
        GM.IsGamePlay = true;
        foreach (var photoTool in PhotoTools)
        {
            photoTool.Clear();
        }
        GM.StartGirl();
    }

    public void SetCuvete(PhotoTool tool)
    {
        SelectTool = tool;
        StartCoroutine(MoveNewCuvette());
    }

    IEnumerator MoveNewCuvette()
    {
        float speed = 2f;
        PhotoItem[] items = ContainerCuvet.GetComponentsInChildren<PhotoItem>();
        foreach (var item in items)
        {
            item.transform.parent = ParentCuvet;
            item.transform.localPosition = Vector3.zero;
        }

        foreach (var item in SelectTool.Items)
        {
            item.transform.parent = ContainerCuvet;
        }

        _scrollCuvettePanel.horizontalNormalizedPosition = 1f;
        while (true)
        {
            _scrollCuvettePanel.horizontalNormalizedPosition -= Time.deltaTime * speed;

            yield return null;
            if (_scrollCuvettePanel.horizontalNormalizedPosition <= 0)
            {
                _isMoveManager = false;
                yield break;
            }
        }

        yield break;
    }

    private void CorrectSizeCamera()
    {
        float proportion;
        float delta = 0.04f;
        if (Screen.width > Screen.height) proportion = (float) Screen.width / Screen.height;
        else proportion = (float) Screen.height / Screen.width;

        float[] mass1 = {1.33f, 1.66f, 1.77f, 2.055f, 2.22f};
        float[] mass2 = {0.2f, 0.35f, 0.4f, 0.47f, 0.5f};

        for (int i = 0; i < mass1.Length; i++)
        {
            if (proportion > mass1[i] - delta && proportion < mass1[i] + delta)
            {
                Camera.rect = new Rect(Camera.rect.x, mass2[i], Camera.rect.width, Camera.rect.height);
                return;
            }
        }
    }

    public void PhotoButton()
    {
        if (CurrentEffect)
        {
            ParticleSystem[] particleSystems = CurrentEffect.GetComponentsInChildren<ParticleSystem>();
            foreach (var system in particleSystems)
            {
                system.GetComponent<Renderer>().sortingLayerName = "Default";
            }
        }
        GameManager.Instance.Winking.StopWatch();
        Photo2.SetActive(true);
        StickerPanelTools.Deactivate_Click();
        Camera.enabled = true;
        Photo.sprite = SaveImageToMemory.CreateScreenShoot_Sprite(Camera);
        Camera.enabled = false;
        Effect.SetActive(true);
        AlbomButton.SetActive(false);
        FlashEffectActivate();
    }
    
    private void FlashEffectActivate()
    {
        GM.PlaySound(FlashSoundEffect);
        StarEffect.transform.localScale=Vector3.one;
        StarEffect.SetActive(true);
        StarEffect.transform.DOScale(Vector3.one * 30f, 0.2f).SetEase(Ease.OutFlash).SetLoops(2,LoopType.Yoyo).onComplete +=
            AnimPhoto;
    }

    private void AnimPhoto()
    {
        FlashEffectDeactivate();
        Photo.transform.parent.position = _startPositionFrame;
        Photo.transform.parent.localScale = Vector3.one/4f;
        Photo.transform.parent.eulerAngles = Vector3.zero;
        Photo.transform.parent.DOScale(Vector3.one, 1f).SetEase(Ease.OutElastic).onComplete += ActivateButtonSave;
    }
    
    private void ActivateButtonSave()
    {
        Effect.SetActive(false);
        CencelButton.SetActive(true);
        SaveButton.SetActive(true);
    }

    private void FlashEffectDeactivate()
    {
        StarEffect.SetActive(false);
        StarEffect.transform.localScale=Vector3.one;
    }

    

    public void SavePhotoClick()
    {
        float speed = 0.6f;
        AlbomButton.SetActive(true);
        AlbomButton.GetComponent<Button>().enabled = false;
        AlbomButton.GetComponent<Animator>().enabled = false;
        AlbomButton.transform.localScale=Vector3.one;
        Photo.transform.parent.DOMove((Vector2)Photo.transform.parent.position+Vector2.up, speed).SetEase(Ease.OutCubic).onComplete +=
            MovePhoto;
        CencelButton.SetActive(false);
        SaveButton.SetActive(false);
        Photo_Album.Instance.PhotoPanel.SetActive(true);
        SaveImageToMemory.SaveTexture(Photo.sprite.texture);
        Photo_Album.Instance.AddPhoto(Photo.sprite);
    }

    private void MovePhoto()
    {
        float speed = 0.3f;
        Photo.transform.parent.DOMove(AlbomButton.transform.position, speed).SetEase(Ease.Flash).onComplete +=
            NextToLevel4;
        Photo.transform.parent.DOScale(Vector3.one / 12f, speed).SetEase(Ease.Flash);
    }

    private void NextToLevel4()
    {
        Photo.transform.parent.DOKill();
        AlbomButton.GetComponent<Button>().enabled = true;
        AlbomButton.GetComponent<Animator>().enabled = true;
    }

    public void AlbumClick()
    {
        Album.Activate(Photo.sprite);
    }

    public void CancelPhotoClick()
    {
        Photo2.SetActive(false);
        Effect.SetActive(false);
        CencelButton.SetActive(false);
        SaveButton.SetActive(false);
        if (CurrentEffect)
        {
            ParticleSystem[] particleSystems = CurrentEffect.GetComponentsInChildren<ParticleSystem>();
            foreach (var system in particleSystems)
            {
                system.GetComponent<Renderer>().sortingLayerName = "CircleItems";
            }
        }
        Photo_Album.Instance.PhotoPanel.SetActive(false);
    }

    public void HomeClick()
    {
        StartMenu.Instance.StartGame();
        Deactivate();
    }

    public void ShopClick()
    {
        NotificationCenter.postNotification(Notifications.OpenStore, null);
    }

    public void BackGame()
    {
        Deactivate();
        DownMenu.ActiveTrue();
    }

    
}