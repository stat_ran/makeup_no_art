using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotoItem : BaseBahavior
{
    public enum PhotoEnum
    {
        Fon, Sticker, Effect
    }

    private PhotoEnum _type;
    private PhotoTool _tool;
    private Image _outline;
    [HideInInspector] public Image Icon;
    [HideInInspector] public GameObject PrefabSticker;
    [HideInInspector] public GameObject Effect;
    
    

    private void Init()
    {
        _outline = GetComponent<Image>();
        _outline.enabled = false;
        Icon = transform.GetChild(0).GetComponent<Image>();
        GetComponent<Button>().onClick.AddListener(Activate);
    }

    public void InitFon(PhotoTool tool, Sprite sprite)
    {
        Init();
        _type = tool.Type;
        _tool = tool;
        Icon.sprite = sprite;
    }
    
    public void InitSticker(PhotoTool tool, Sprite sprite, GameObject prefab)
    {
        Init();
        _type = tool.Type;
        _tool = tool;
        _outline.sprite = sprite;
        Icon.sprite = sprite;
        PrefabSticker = prefab;
    }
    
    public void InitEffect(PhotoTool tool, Sprite icon, GameObject Effect)
    {
        Init();
        _type = tool.Type;
        _tool = tool;
        Icon.sprite = icon;
        this.Effect = Effect;
        _outline.sprite = icon;
    }

    public void Activate()
    {
        // if (_tool.SelectItem == this)
        // {
        //     Deactivate();
        //     _tool.SelectItem = null;
        //     _tool.SetItem(null);
        //     return;
        // }
        // if(_tool.SelectItem) _tool.SelectItem.Deactivate();
        // _tool.SelectItem = this;
        // _outline.enabled = true;
        // transform.localScale = Vector3.one * 1.2f;
        _tool.SetItem(this);
    }

    public void Deactivate()
    {
        _outline.enabled = false;
        transform.localScale = Vector3.one;
    }
    
}
