﻿using UnityEngine;

public class BrowsBrush : Tool
{
    public void TouchDrag()
    {
        //MarkOutline(false);
        foreach (SpriteRenderer sr in TargetImages)
        {
            GM.Paint.DrawImage(sr, SelectedImage, PrevPos + Offset, SelectedColor, false);
        }
    }
}
