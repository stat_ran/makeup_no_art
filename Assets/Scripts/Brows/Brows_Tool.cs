using UnityEngine;

public class Brows_Tool : Tool, IDrawTool
{
    [HideInInspector] public Sprite Sprite;
   
    public void Init(Manager manager, Sprite sprite, Sprite icon)
    {
        base.Init(manager);
        DeltaLeftPosition = 0.12f;
        IsEraser = true;
        DrawTool = this;
        Sprite = sprite;
        IconTool.sprite = icon;
        foreach (var colorCuvette in Data.ColorData.ColorCuvettes)
        {
            GameObject obj = Instantiate(Data.BrowsData.PrefabCuvette, DownMenu.ParentCuvet);
            obj.transform.localPosition=Vector3.zero;
            Item item = obj.GetComponent<Item>();
            Items.Add(item);
            item.Init(this, colorCuvette.Icon);
            item.SetColor(colorCuvette.Color);
        }
    }

    public override void Activate()
    {
        base.Activate();
        DownMenu.UpCuvettePanel(this);
        if(Manager) Manager.ActivateTutor();
    }
    
    public void Draw()
    {
        if (SelectedItem)
        {
            foreach (var targetImage in TargetImages)
            {
                Paint.DrawImage(targetImage, Sprite, MousePosition, SelectedItem.Color);
            }
        }
    }
}
