﻿using UnityEngine;

public class Brows_Manager : Manager
{
    public override void Start()
    {
        base.Start();
        foreach (var brows in Data.BrowsData.Browses)
        {
            GameObject obj = Instantiate(Data.BrowsData.PrefabTool, DownMenu.ParentTools);
            obj.transform.localPosition=Vector3.zero;
            obj.GetComponent<Brows_Tool>().Init(this, brows.Sprite, brows.Icon);
            Tools.Add(obj.GetComponent<Tool>());
        }
    }

    public override void Randomizer()
    {
        base.Randomizer();
        return;
        // Sprite sp = Data.BrowsData.Browses[Random.Range(0, Data.BrowsData.Browses.Length)].Sprite;
        // RectInt rect = new RectInt((int)sp.textureRect.xMin, (int)sp.textureRect.yMin, (int)sp.textureRect.width, (int)sp.textureRect.height);
        // Color[] pixels = sp.texture.GetPixels(rect.x, rect.y, rect.width, rect.height);
        // Color color = Color.black; //Colors.GetChild(Random.Range(0, 5)).GetComponent<Item_Color>().Color;
        //
        // for (int i = 0; i < pixels.Length; i++)
        //     pixels[i] = new Color(Mathf.Clamp(pixels[i].r, 0, color.r),      
        //         Mathf.Clamp(pixels[i].g, 0, color.g),
        //         Mathf.Clamp(pixels[i].b, 0, color.b),
        //         pixels[i].a);
        //
        // foreach (SpriteRenderer target in TargetImages)
        // {
        //     target.sprite.texture.SetPixels(pixels);
        //     target.sprite.texture.Apply();
        // }
    }
    
}
