﻿using AceCream.Scripts.Notification;
using UnityEngine;

public class EarringsTool : Tool
{
    [HideInInspector] public SpriteRenderer EarringLeft;
    [HideInInspector] public SpriteRenderer EarringRight;

    private Sprite _sprite;

    public override void Start()
    {
        base.Start();

        IsMovementAllowed = false;
        NotificationCenter.addCallback(Notifications.CheckBuy, CheckBuy);
    }
    
    private void OnDestroy()
    {
        NotificationCenter.removeCallbeck(Notifications.CheckBuy, CheckBuy);
    }
    
    public void Init(SpriteRenderer leftRenderer, SpriteRenderer rightRenderer, Sprite sprite, Sprite icon, Sprite outline, Manager manager, int lockNumber)
    {
        MyAwake();
        ManagersEnum = ManagersEnum.Earring;
        Manager = manager;
        _sprite = sprite;
        EarringLeft = leftRenderer;
        EarringRight = rightRenderer;
        IconTool.sprite = icon;
        Outline.sprite = outline;
        LockNumber = lockNumber;
        CheckBuy(null);
        gameObject.SetActive(false);
    }

    public override void Activate()
    {
        if (IsLock)
        {
            MyShowRewardAds.Instance.Activate(GetName(), this);
            return;
        }
        base.Activate();
        EarringLeft.sprite = _sprite;
        EarringRight.sprite = _sprite;
        DownMenu.StartAnimArrow();
    }
    
}
