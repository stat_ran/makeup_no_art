﻿using UnityEngine;

public class Earring_Manager : Manager
{
    public SpriteRenderer EarringLeft;
    public SpriteRenderer EarringRight;
    
    public override void Start()
    {
        base.Start();
        IsSpawnTools = true;
        DeltaLeftPosition = 0.02f;
        foreach (var earring in Data.EarringData.Earrings)
        {
            GameObject obj = Instantiate(Data.EarringData.Prefab, DownMenu.ParentTools);
            obj.transform.localPosition=Vector3.zero;
            obj.GetComponent<EarringsTool>().Init(EarringLeft, EarringRight, earring.Sprite, earring.Icon, earring.Outline, this, earring.LockNumber);
            Tools.Add(obj.GetComponent<Tool>());
        }
    }

    public override void Randomizer()
    {
        base.Randomizer();
        Sprite sp =  Data.EarringData.Earrings[Random.Range(0,5)].Sprite;
        EarringLeft.sprite = sp;
        EarringRight.sprite = sp;
    }

    public override void Activate()
    {
        base.Activate();
        foreach (var spriteRenderer in TutorSpriteRenderers)
        {
            spriteRenderer.sprite = EarringLeft.sprite;
        }
        ActivateTutor();
    }
}
