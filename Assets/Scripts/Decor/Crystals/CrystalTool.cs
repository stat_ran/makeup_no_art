﻿using AceCream.Scripts.Notification;
using UnityEngine;

public class CrystalTool : Tool, IDrawTool
{
    [HideInInspector] public GameObject Prefab;
    [HideInInspector] public Collider2D Mask;

    private int _order = 0;
    private int _layer = 1 << 12;

    public override void Start()
    {
        base.Start();

        IsEraser = true;
        NotificationCenter.addCallback(Notifications.CheckBuy, CheckBuy);
    }
    
    private void OnDestroy()
    {
        NotificationCenter.removeCallbeck(Notifications.CheckBuy, CheckBuy);
    }

    public void Init(Manager manager, GameObject prefab, Collider2D mask, int lockNumber)
    {
        MyAwake();
        base.Init(manager);
        DrawTool = this;
        Prefab = prefab;
        Mask = mask;
        LockNumber = lockNumber;
        CheckBuy(null);
    }

    public void Draw()
    {
        if(!Physics2D.Raycast(MousePosition, Vector2.zero, 0, _layer)) return;
        CreateCrystal(MousePosition);
    }


    public GameObject CreateCrystal(Vector2 pos)
    {
        var crystal = Instantiate(Prefab, pos, Quaternion.identity);
        crystal.name = "Crystal";
        crystal.GetComponent<SpriteRenderer>().sortingOrder = ++_order;
        crystal.transform.localScale=Vector3.one*0.5f;
        crystal.transform.parent = Manager.transform;
        return crystal;
    }

    public override void Activate()
    {
        if (IsLock)
        {
            MyShowRewardAds.Instance.Activate(GetName(), this);
            return;
        }
        base.Activate();
    }
}
