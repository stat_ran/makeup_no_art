﻿using UnityEngine;

public class Crystal_Manager : Manager
{
    
    public Collider2D Mask;

    public override void Start()
    {
        base.Start();
        IsSpawnTools = true;
        DeltaLeftPosition = 0.035f;
        foreach (var crystal in Data.CrystalData.Crystals)
        {
            GameObject obj = Instantiate(crystal.ScrollPrefab, DownMenu.ParentTools);
            obj.transform.localPosition=Vector3.zero;
            obj.GetComponent<CrystalTool>().Init(this, crystal.Prefab, Mask, crystal.LockNumber);
            Tools.Add(obj.GetComponent<Tool>());
        }   
    }

    public override void Activate()
    {
        base.Activate();
        Mask.enabled = true;
    }

    public override void Deactivate()
    {
        base.Deactivate();
        Mask.enabled = false;
    }
    

    public override void Clear()
    {
        base.Clear();

        foreach (var crystal in GameObject.FindGameObjectsWithTag("Crystal"))
            if (crystal.name != "Example")
                Destroy(crystal);
    }
}
