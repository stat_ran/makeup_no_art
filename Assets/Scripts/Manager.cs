﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Manager : BaseBahavior
{
    public SpriteRenderer[] TargetSpriteRenderers;
    public SpriteRenderer[] TutorSpriteRenderers;
    
    [HideInInspector] public Item Item;
    [HideInInspector] public Tool Tool;
    [HideInInspector] public Camera MyCamera;
    [HideInInspector] public bool IsSpawnTools; //Спавним копии инструмента в которых меняются только спрайты
    [HideInInspector] public float DeltaLeftPosition;

    public List<Tool> Tools=new List<Tool>();
    
    private Color _colorClear=new Color(1f,1f,1f,0);

    public virtual void Start()
    {
        GM = GameManager.Instance;
        MyCamera=Camera.main;
        ClearTutor();
    }

    public virtual void Activate()
    {
        foreach (var tool in Tools)
        {
            if (tool.IsEraser)
            {
                DownMenu.Eraser.ShowEraser();
                return;
            }
        }
        DownMenu.Eraser.Hide();
    }

    public virtual void Deactivate()
    {
        ClearTutor();
    }

    public virtual void Init()
    {
        GM.SetManager(this);

        foreach (Tool tool in Tools)
            tool.Activate();

        SelectedTool(Tools[0]);
    }

    public virtual void DeInit()
    {
        foreach (Tool tool in Tools)
            tool.Deactivate();
        Tool = null;

        GM.SetManager(null);
    }

    public virtual void Clear()
    {
        foreach (var spriteRenderer in TargetSpriteRenderers)
        {
            Paint.ClearImage(spriteRenderer);
        }
    }

    public virtual void Blinking()
    {
        
    }

    public virtual void SelectedTool(Tool tool)
    {
        if (Tool != tool)
        {
            TutorTools tutorTools = tool.GetComponent<TutorTools>();
            if(tutorTools!=null) tutorTools.Activate();
        }
        
        Tool = tool;
        //GM.Eraser.DeActivate();
    }
    
    public virtual void Randomizer()
    {
        
    }

    public void ActivateTutor()
    {
        foreach (var spriteRenderer in TutorSpriteRenderers)
        {
            if( DOTween.IsTweening(spriteRenderer)) return;
            spriteRenderer.DOColor(Color.white, 0.2f).SetEase(Ease.Linear).SetLoops(6, LoopType.Yoyo);
        }
    }

    public void ClearTutor()
    {
        foreach (var spriteRenderer in TutorSpriteRenderers)
        {
            spriteRenderer.DOKill();
            spriteRenderer.color = _colorClear;
        }
    }

    public virtual void IsPaintGirl(bool isPaint)
    {
        if (isPaint)
        {
            DownMenu.StartAnimArrow();
        }
        else
        {
            DownMenu.StopAnimArrow();
        }
        
    }

}
