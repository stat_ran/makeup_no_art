﻿using UnityEngine;

public class Mascara : Tool
{
    private SpriteRenderer sr;

    public override void Start()
    {
        base.Start();

        sr = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    public void TouchDrag()
    {
        //MarkOutline(false);
        if (!SelectedImage) return;
        foreach (SpriteRenderer sr in TargetImages)
        {
            GM.Paint.DrawImage(sr, SelectedImage, PrevPos + Offset, SelectedColor, false);
        }
    }

    // public override void SetColor(Color color)
    // {
    //     base.SetColor(color);
    //
    //     sr.color = color;
    // }
}
