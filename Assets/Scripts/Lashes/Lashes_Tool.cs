using System.Collections;
using System.Collections.Generic;
using AceCream.Scripts.Notification;
using UnityEngine;

public class Lashes_Tool : Tool, IDrawTool //Ресницы!!!
{
   
   [HideInInspector] public Sprite Sprite;

   public override void Start()
   {
      base.Start();
      DeltaLeftPosition = 0.1f;
      NotificationCenter.addCallback(Notifications.CheckBuy, CheckBuy);
   }
   
   private void OnDestroy()
   {
      NotificationCenter.removeCallbeck(Notifications.CheckBuy, CheckBuy);
   }

   public void Init(Manager manager,Sprite sprite, Sprite icon, int lockNumber)
   {
      MyAwake();
      base.Init(manager);
      IsEraser = true;
      DrawTool = this;
      Sprite = sprite;
      LockNumber = lockNumber;
      IconTool.sprite = icon;
      foreach (var colorCuvette in Data.ColorData.ColorCuvettes)
      {
         GameObject obj = Instantiate(Data.LashesData.PrefabCuvette, DownMenu.ParentCuvet);
         obj.transform.localPosition=Vector3.zero;
         Item item = obj.GetComponent<Item>();
         Items.Add(item);
         item.Init(this, colorCuvette.Icon);
         item.SetColor(colorCuvette.Color);
      }
      CheckBuy(null);
   }

   public override void Activate()
   {
      if (IsLock)
      {
         MyShowRewardAds.Instance.Activate(GetName(), this);
         return;
      }
      base.Activate();
      DownMenu.UpCuvettePanel(this);
      Manager.ActivateTutor();
   }
   
   public void Draw()
   {
      if (SelectedItem)
      {
         foreach (var targetImage in TargetImages)
         {
            Paint.DrawImage(targetImage, Sprite, MousePosition, SelectedItem.Color);
         }
      }
   }
}
