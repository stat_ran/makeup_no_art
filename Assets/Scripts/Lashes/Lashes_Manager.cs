﻿using UnityEngine;

public class Lashes_Manager : Manager
{
    
    public override void Start()
    {
        base.Start();
        
        IsSpawnTools = true;
        DeltaLeftPosition = 0.1f;
        foreach (var lashes in Data.LashesData.Lasheses)
        {
            GameObject obj = Instantiate(Data.LashesData.PrefabTool, DownMenu.ParentTools);
            obj.transform.localPosition=Vector3.zero;
            obj.GetComponent<Lashes_Tool>().Init(this, lashes.Sprite, lashes.Icon, lashes.LockNumber);
            Tools.Add(obj.GetComponent<Tool>());
        }

        foreach (var spriteRenderer in TargetSpriteRenderers)
        {
            Paint.ClearImage(spriteRenderer);
        }
    }

    // public override void Clear()
    // {
    //     base.Clear();
    //
    //     //foreach (SpriteRenderer sr in _lashes)
    //     //    GM.Paint.ClearImage(sr);
    // }
}
