﻿using System;
using System.Collections.Generic;
using AceCream.Scripts.Notification;
using UnityEngine;
using UnityEngine.EventSystems;

public class Paint : BaseBahavior
{
	public SpriteRenderer[] paintImages;
	public int PenRadius = 60;

	private int _layer = 1 << 11;
	[HideInInspector] public bool IsPaint;

	public override void Awake()
	{
		base.Awake();
		foreach (var images in paintImages)
			images.sprite = SafeCopyTexture(images.sprite);
	}

	private void Update()
	{
		if(DownMenu.IsClear || !GM.IsGamePlay) return;
		
		if (Input.GetMouseButton(0)) 
		{
			if (PointerRaycast(MousePosition))
			{
				if (DownMenu.Manager && DownMenu.Manager.Tool && !(DownMenu.Manager.Tool is CrystalTool))
				{
					DownMenu.Manager.IsPaintGirl(IsPaint);
					DownMenu.Manager.Tool.DrawTool?.Draw();
					GameManager.Instance.Winking.EyeWatch(MousePosition);
				}
			}
		}
		
		if (Input.GetMouseButtonDown(0)) 
		{
			if (PointerRaycast(MousePosition))
			{
				if (DownMenu.Manager && DownMenu.Manager.Tool && (DownMenu.Manager.Tool is CrystalTool))
				{
					IsPaint = false;
					DownMenu.Manager.Tool.DrawTool?.Draw();
					GameManager.Instance.Winking.EyeWatch(MousePosition);
				}
			}
		}

		if (Input.GetMouseButtonUp(0))
		{
			if (PointerRaycast(MousePosition))
			{
				DownMenu.Manager.IsPaintGirl(IsPaint);
				IsPaint = false;
				GameManager.Instance.Winking.StopWatch();
				GameManager.Instance.Winking.SwitchWink(false);
			}
		}

	}
	
	bool PointerRaycast(Vector2 position)
	{
		PointerEventData pointerData = new PointerEventData(EventSystem.current);
		pointerData.position = position;
#if !UNITY_EDITOR
		if(EventSystem.current.IsPointerOverGameObject(0)) return false;
		else return true;
#endif
		
#if UNITY_EDITOR
		if (EventSystem.current.IsPointerOverGameObject()) return false;
		else return true;
#endif
	}

	public void ClearImage(SpriteRenderer renderer, bool isSave = true)
	{
		if (!renderer.sprite) return;
		if (!isSave)
		{
			Color32[] clearColors = new Color32[renderer.sprite.texture.width * renderer.sprite.texture.height];
			for (int i = 0; i < clearColors.Length; i++)
				clearColors[i] = new Color32(0, 0, 0, 0);
			renderer.sprite.texture.SetPixels32(clearColors);
			renderer.sprite.texture.Apply();
		}
		else
		{
			Color32[] pixels = renderer.sprite.texture.GetPixels32();
			for (int i = 0; i < pixels.Length; i++)
				pixels[i].a = 0;
			renderer.sprite.texture.SetPixels32(pixels);
			renderer.sprite.texture.Apply();
		}
	}

    public Sprite SafeCopyTexture(Sprite sprite)
    {
	    RectInt rect = new RectInt((int)sprite.textureRect.xMin, (int)sprite.textureRect.yMin, (int)sprite.textureRect.width, (int)sprite.textureRect.height);
		Texture2D texture2D = new Texture2D(rect.width, rect.height);
        texture2D.SetPixels32(new Color32[texture2D.width * texture2D.height]);
        texture2D.Apply();

		Sprite newSprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), Vector2.one / 2, sprite.pixelsPerUnit);
		newSprite.texture.SetPixels(sprite.texture.GetPixels(rect.x, rect.y, rect.width, rect.height));
        texture2D.Apply();

        return newSprite;
	}

	public void DrawImage(SpriteRenderer targetImage, Sprite selectedImage) //, Vector2 touchPoint
    {
	    if(!targetImage || !selectedImage) return;
	    
	    DrawImage(targetImage, selectedImage, MousePosition, Color.white);
	    
    }

	public void DrawImage(SpriteRenderer targetImage, Sprite selectedImage, Vector2 touchPoint, Color selectedColor, bool isMixing = true)
    {
	    Sprite sprite = targetImage.sprite;
		float width = sprite.texture.width;
		float height = sprite.texture.height;
		Transform spTransform = targetImage.transform;

		Vector2 point = Solving(spTransform.right, spTransform.up, touchPoint - (Vector2)spTransform.position);
		Vector2 pixelPoint = new Vector2(point.x / spTransform.localScale.x * sprite.pixelsPerUnit + sprite.texture.width / 2,
										 point.y / spTransform.localScale.y * sprite.pixelsPerUnit + sprite.texture.height / 2);
		pixelPoint = new Vector2(targetImage.flipX ? sprite.texture.width - pixelPoint.x : pixelPoint.x,
								 targetImage.flipY ? sprite.texture.height - pixelPoint.y : pixelPoint.y);

		if (pixelPoint.magnitude > PenRadius + sprite.texture.width + sprite.texture.height) return;
		
	    for (int i = -PenRadius; i < PenRadius; i++)
		{
			for (int j = -PenRadius; j < PenRadius; j++)
			{
				float dist = Vector2.Distance(Vector2.zero, new Vector2(i, j));
				if (dist > PenRadius) continue;
				Vector2 vec = new Vector2(pixelPoint.x + i, pixelPoint.y + j);
				if (vec.x < 0 || vec.x > width || vec.y < 0 || vec.y > height) continue;
				IsPaint = true;
				Color pixel = selectedImage.texture.GetPixel((int)(selectedImage.textureRect.x + vec.x), (int)(selectedImage.textureRect.y + vec.y));
				
                if (selectedColor != Color.white)
	                pixel = new Color(selectedColor.r, selectedColor.g, selectedColor.b, pixel.a);
				if (isMixing)
                {
					Color firstPixel = sprite.texture.GetPixel((int)vec.x, (int)vec.y);
					Color newPixel = Color.Lerp(pixel, firstPixel, dist / PenRadius - 0.4f);
					sprite.texture.SetPixel((int)vec.x, (int)vec.y, newPixel);
				}
				else
					sprite.texture.SetPixel((int)vec.x, (int)vec.y, pixel);
			}
		}

		sprite.texture.Apply();
		
		int layer = 1 << 10;
		RaycastHit2D[] hit2D = Physics2D.RaycastAll(touchPoint, Vector2.zero, 0, layer);

		if(hit2D.Length==0) GameManager.Instance.CurrentPaintSprite = targetImage;
		else
		{
			foreach (var raycastHit2D in hit2D)
			{
				if (raycastHit2D.collider.GetComponent<SpriteRenderer>() == targetImage)
				{
					GameManager.Instance.CurrentPaintSprite = targetImage;
				}
			}
		}
		
		GameManager.Instance.ColorTools = selectedColor;
    }

	public void DrawAllImage(SpriteRenderer targetImage, Sprite selectedImage, Color color)
	{
		if (color == Color.white)
		{
			targetImage.sprite.texture.SetPixels(selectedImage.texture.GetPixels());
		}
		else
		{
			Color[] colors = selectedImage.texture.GetPixels();
			for (int i = 0; i < colors.Length; i++)
			{
				colors[i]=new Color(color.r, color.g, color.b, colors[i].a);
			}
			targetImage.sprite.texture.SetPixels(colors);
		}
		targetImage.sprite.texture.Apply();
	}

	public void EraseImage(SpriteRenderer targetImage, Vector2 touchPoint, int eraseRadius)
    {
		Sprite sprite = targetImage.sprite;
		Transform spTransform = targetImage.transform;

		Vector2 point = Solving(spTransform.right, spTransform.up, touchPoint - (Vector2)spTransform.position);
		Vector2Int pixelPoint = new Vector2Int((int)(point.x / spTransform.localScale.x * sprite.pixelsPerUnit + sprite.texture.width / 2f),
										 (int)(point.y / spTransform.localScale.y * sprite.pixelsPerUnit + sprite.texture.height / 2f));
		pixelPoint = new Vector2Int(targetImage.flipX ? sprite.texture.width - pixelPoint.x : pixelPoint.x,
								 targetImage.flipY ? sprite.texture.height - pixelPoint.y : pixelPoint.y);

		if (pixelPoint.magnitude > eraseRadius + (sprite.texture.width + sprite.texture.height) / 2) return;
		for (int i = -eraseRadius; i < eraseRadius; i++)
		{
			for (int j = -eraseRadius; j < eraseRadius; j++)
			{
				if (Vector2.Distance(Vector2.zero, new Vector2(i, j)) > eraseRadius) continue;
				Vector2Int vec = new Vector2Int(pixelPoint.x + i, pixelPoint.y + j);
				if (vec.x < 0 || vec.x > sprite.texture.width) continue;
				if (vec.y < 0 || vec.y > sprite.texture.height) continue;
				Color pixel = sprite.texture.GetPixel(vec.x, vec.y);
				pixel.a = 0;
				sprite.texture.SetPixel(vec.x, vec.y, pixel);
			}
		}

		sprite.texture.Apply();
	}

	private Vector2 Solving(Vector2 right, Vector2 up, Vector2 pos)
	{
		float valX = (up.x * pos.y - up.y * pos.x) / (up.x * right.y - up.y * right.x);
		float valY = (right.x * pos.y - right.y * pos.x) / (right.x * up.y - right.y * up.x);
		return new Vector2(valX, valY);
	}
	
}
