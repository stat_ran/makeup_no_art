
public class Blush_Manager : Manager
{
    public override void Awake()
    {
        base.Awake();
        foreach (var tool in Tools)
        {
            tool.Init(this);
        }
    }

    public override void Activate()
    {
        base.Activate();
        ActivateTutor();
    }

    public override void Clear()
    {
        base.Clear();
        
        //foreach (SpriteRenderer sr in Blush)
        //    GM.Paint.ClearImage(sr);
    }
}
