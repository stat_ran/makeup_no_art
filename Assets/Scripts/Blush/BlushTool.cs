﻿using UnityEngine;

public class BlushTool : Tool, IDrawTool
{
    public override void Start()
    {
        base.Start();
        DrawTool = this;
        foreach (var blush in Data.BlushData.Blushes)
        {
            GameObject obj = Instantiate(Data.BlushData.Prefab, DownMenu.ParentCuvet);
            obj.transform.localPosition=Vector3.zero;
            Item item = obj.GetComponent<Item>();
            Items.Add(item);
            item.Init(this, blush.Icon);
            item.SetSprite(blush.Sprite);
        }
    }

    public override void Activate()
    {
        base.Activate();
        DownMenu.UpCuvettePanel(this);
    }
    
    public void Draw()
    {
        if (SelectedItem)
        {
            foreach (var targetImage in TargetImages)
            {
                Paint.DrawImage(targetImage, SelectedItem.Sprite);
            }
        }
    }
}
