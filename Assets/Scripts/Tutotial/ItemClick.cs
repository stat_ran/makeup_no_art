using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemClick : MonoBehaviour
{

    public string NameTool;
    private TutorTools _tutorTools;
    public ParticleSystem[] ParticleSystem;

    private float _time;
    
    void Start()
    {
        _tutorTools = GameObject.Find(NameTool)?.GetComponent<TutorTools>();
    }

    private void OnMouseDown()
    {
        _time = Time.time;
    }

    private void OnMouseUp()
    {
        if (Time.time - _time > 0.2f)
        {
            _time = Time.time;
            return;
        }
        if(_tutorTools!=null) _tutorTools.StartAnim();
        foreach (var system in ParticleSystem)
        {
            system.Play();
        }
        GameManager.Instance.AudioSource.PlayOneShot(GameManager.Instance.StarAudioClip);
    }
}
