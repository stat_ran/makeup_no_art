using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Tutorial : MonoBehaviour
{

    public enum TypeTutor
    {
        Bar, Kuveta, Face
    }
    
    public List<ElementTutor> ElementTutors=new List<ElementTutor>();

    public GameObject HandBar;
    public GameObject HandFace;
    public GameObject HandKuveta;
    public GameObject HandStone;

    public Transform KuvetaPosition;

    [HideInInspector] public TypeTutor CurentTutorType;

    private float _time;
    private bool _isPlay;
    private Vector2 _startScale=Vector2.zero;
    public int CurrentBar;

    void Start()
    {
        CurentTutorType = TypeTutor.Bar;
        _time = Time.time;
        HandBar.SetActive(false);
        HandFace.SetActive(false);
        HandKuveta.SetActive(false);
        HandStone.SetActive(false);
    }

    
    void Update()
    {
        return;
        if (!GameManager.Instance.IsGamePlay)
        {
            _time = Time.time;
            return;
        }
        if (!_isPlay && Time.time - _time > 5f)
        {
            StartTutor();
        }

        if (Input.GetMouseButton(0))
        {
            _time = Time.time;
            StopTutor();
        }
    }

    private void StartTutor()
    {
        _isPlay = true;
        switch (CurentTutorType)
        {
            case TypeTutor.Bar:
                //HandBar.SetActive(true);
                //HandBar.transform.position = ElementTutors[CurrentBar].Bar.position;
                _startScale = ElementTutors[CurrentBar].Bar.localScale;
                ElementTutors[CurrentBar].Bar.DOScale(_startScale * 1.2f, 0.5f).SetLoops(-1, LoopType.Yoyo)
                    .SetEase(Ease.Linear);
                break;
            case TypeTutor.Kuveta:
                HandKuveta.SetActive(true);
                HandKuveta.transform.position = KuvetaPosition.position;
                break;
            case TypeTutor.Face:
                HandFace.SetActive(true);
                if (ElementTutors[CurrentBar].Face == null)
                {
                    HandFace.SetActive(false);
                    SetNextTutor();
                    StartTutor();
                }
                else
                {
                    if (CurrentBar == 7)
                    {
                        HandFace.SetActive(false);
                        HandStone.SetActive(true);
                    }
                    else HandFace.transform.position = ElementTutors[CurrentBar].Face.position;
                }
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        //SetNextTutor();
    }

    private void StopTutor()
    {
        if(!_isPlay) return; 
        HandBar.SetActive(false);
        HandFace.SetActive(false);
        HandKuveta.SetActive(false);
        HandStone.SetActive(false);
        _isPlay = false;
        ElementTutors[CurrentBar].Bar.DOKill();
        if(_startScale!=Vector2.zero) ElementTutors[CurrentBar].Bar.localScale = _startScale;
    }

    private void SetNextTutor()
    {
        if (CurentTutorType == TypeTutor.Bar)
        {
            CurentTutorType = TypeTutor.Kuveta;
            return;
        }

        if (CurentTutorType == TypeTutor.Kuveta)
        {
            CurentTutorType = TypeTutor.Face;
            return;
        }

        if (CurentTutorType == TypeTutor.Face)
        {
            CurentTutorType = TypeTutor.Bar;
        }
    }

}

[System.Serializable]
public class ElementTutor
{
    public Transform Face;
    public Transform Bar;
}
