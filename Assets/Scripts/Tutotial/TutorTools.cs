using System;
using System.Collections;
using System.Collections.Generic;
using AceCream.Scripts.Notification;
using DG.Tweening;
using UnityEditor;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class TutorTools : MonoBehaviour
{

    public SpriteRenderer[] TargetSprite;
    public SpriteRenderer[] HelpSprite;
    public TutorTools TargetTools;
    public bool IsBlush;
    public bool IsHair;

    private GameManager _gameManager;
    private Vector2 _startScale;
    private Tool _tools;
    

    private void FixedUpdate()
    {
        if (IsHair)
        {
            for (int i = 0; i < TargetSprite.Length; i++)
            {
                HelpSprite[i].sprite = TargetSprite[i].sprite;
            }
        }
    }

    private void Start()
    {
        _gameManager=GameManager.Instance;
        _tools = GetComponent<Tool>();
        //_animator = GetComponent<Animator>();
        _startScale = IsBlush ? transform.parent.localScale : transform.localScale;
        foreach (var spriteRenderer in HelpSprite)
        {
            spriteRenderer.color=Color.clear;
        }

        if (IsBlush)
        {
            for (int i = 0; i < TargetSprite.Length; i++)
            {
                HelpSprite[i].sprite = SafeCopyTexture(TargetSprite[i].sprite);
                var pixels = HelpSprite[i].sprite.texture.GetPixels();
                for (var j = 0; j < pixels.Length; j++)
                {
                    pixels[j].a *= 12;
                }
                HelpSprite[i].sprite.texture.SetPixels(pixels);
                HelpSprite[i].sprite.texture.Apply();
            }
        }
        NotificationCenter.addCallback(Notifications.SelectTool, Deactivate);
    }

    private void OnDestroy()
    {
        NotificationCenter.removeCallbeck(Notifications.SelectTool, Deactivate);
    }

    public void Activate()
    {
        StartCoroutine(BlinkCor());
        NotificationCenter.postNotification(Notifications.SelectTool, name);
        //StartAnim();
    }


    IEnumerator BlinkCor()
    {
        Color color=Color.white;
        color.a = 0;
        float speed = 2f;
        foreach (var spriteRenderer in HelpSprite)
        {
            spriteRenderer.color=Color.clear;
        }

        for (int i = 0; i < 2; i++)
        {
            while (color.a < 0.9f)
            {
                color.a += Time.deltaTime * speed;
                foreach (var spriteRenderer in HelpSprite)
                {
                    spriteRenderer.color = color;
                }

                yield return null;
            }

            while (color.a > 0.1f)
            {
                color.a -= Time.deltaTime * speed;
                foreach (var spriteRenderer in HelpSprite)
                {
                    spriteRenderer.color = color;
                }

                yield return null;
            }
        }
        foreach (var spriteRenderer in HelpSprite)
        {
            spriteRenderer.color=Color.clear;
        }

    }

    private void Deactivate(object obj)
    {
        StopAnim();
        if(name==obj as string) return;
        foreach (var spriteRenderer in HelpSprite)
        {
            spriteRenderer.color=Color.clear;
        }
        StopAllCoroutines();
        
    }

    public void StartAnim()
    {
        //Debug.Log("StartAnim");
        if(TargetTools==null) return;
        if (TargetTools.IsBlush)
        {
            TargetTools.transform.parent.DOKill();
            TargetTools.transform.parent.localScale = _startScale;
            TargetTools.transform.parent.DOScale(_startScale * 1.2f, 0.5f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
            return;
        }

        TargetTools.StopAnim();
        TargetTools.transform.DOScale(_startScale * 1.2f, 0.5f).SetLoops(-1, LoopType.Yoyo);
    }

    private void StopAnim()
    {
        //Debug.Log("StoipAnim");
        if (IsBlush)
        {
            transform.parent.DOKill();
            transform.parent.localScale = _startScale;
            return;
        }

        transform.DOKill();
        transform.localScale = _startScale;
    }

    private Sprite SafeCopyTexture(Sprite sprite)
    {
        RectInt rect = new RectInt((int)sprite.textureRect.xMin, (int)sprite.textureRect.yMin, (int)sprite.textureRect.width, (int)sprite.textureRect.height);
        Texture2D texture2D = new Texture2D(rect.width, rect.height);
        texture2D.SetPixels32(new Color32[texture2D.width * texture2D.height]);
        texture2D.Apply();

        Sprite newSprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), Vector2.one / 2, sprite.pixelsPerUnit);
        newSprite.texture.SetPixels(sprite.texture.GetPixels(rect.x, rect.y, rect.width, rect.height));
        texture2D.Apply();

        return newSprite;
    }
    
}
