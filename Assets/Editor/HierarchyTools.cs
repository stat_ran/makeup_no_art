﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace AceCream.Scripts.Editor
{
    [InitializeOnLoad]
    public class HierarchyTools
    {
        private static List<string> _setActiveChekBoxNames = new List<string>()
        {
            "stage",
        };
        
        private static readonly List<string> IgnoreIcons = new List<string>()
        {
            "d_GameObject Icon", 
            "d_Prefab Icon"
        };
        
        static HierarchyTools()
        {
            EditorApplication.hierarchyWindowItemOnGUI += HandleHierarchyWindowItemOnGUI;
        }

        static void HandleHierarchyWindowItemOnGUI(int instanceID, Rect selectionRect)
        {
            var @object = EditorUtility.InstanceIDToObject(instanceID);
            var content = EditorGUIUtility.ObjectContent(@object, null);

            foreach (var name in _setActiveChekBoxNames)
            {
                var go = (GameObject) @object;
                if(go == null) continue;
                if (go.name.ToLower().Contains(name.ToLower()))
                {
                    if (GUI.Button(new Rect(selectionRect.xMax - 18, selectionRect.yMin, 18, 16), "A"))
                    {
                        go.SetActive(!go.activeSelf);
                        EditorSceneManager.MarkSceneDirty(go.scene);
                    }
                }
            }


            if (content.image != null && !IgnoreIcons.Contains(content.image.name))
            { 
                GUI.DrawTexture(new Rect(selectionRect.xMax - 16, selectionRect.yMin, 16, 16), content.image);
                
                /*if (GUI.Button(new Rect(32/*selectionRect.xMax - 16 * 4#1#, selectionRect.yMin, 16, 16), "I"))    
                {
                    Debug.LogError($"{@object.name}");   
                }*/
            }
            
            
            
        }
    }
}